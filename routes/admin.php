<?php

// Set default language to th
App::setLocale('th');

// Routes for admin section
Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::group(['namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('index');

    // For admin management section
    Route::group(['as' => 'admin_management.', 'prefix' => 'admin-management'], function () {
        Route::get('/', 'UserController@index')->name('index');
        Route::get('/create', 'UserController@create')->name('create');
        Route::post('/create', 'UserController@store')->name('store');
        Route::put('/change-password/{user}', 'UserController@changePassword')->name('change_password');
        Route::delete('/{user}', 'UserController@destroy')->name('destroy');
    });

    // For project management section
    Route::group(['as' => 'project_management.', 'prefix' => 'projects'], function () {
        Route::get('/{type}', 'ProjectController@index')->name('index');
        Route::get('/create/{type}', 'ProjectController@create')->name('create');
        Route::get('/exist-create/{type}', 'ProjectController@existCreate')->name('exist_create');
        Route::get('/{type}/{project}/edit', 'ProjectController@edit')->name('edit');
        Route::post('/create', 'ProjectController@store')->name('store');
        Route::post('/exist_create', 'ProjectController@existStore')->name('exist_store');
        Route::put('/{project}', 'ProjectController@update')->name('update');
        Route::delete('/{project}', 'ProjectController@destroy')->name('destroy');
    });

    // For profile section
    Route::group(['as' => 'profile.', 'prefix' => 'profile'], function () {
       Route::get('/', 'ProfileController@index')->name('index');
       Route::post('/change-password', 'ProfileController@changePassword')->name('change_password');
    });

    // Pattern
    Route::group(['as' => 'patterns.', 'prefix' => 'patterns'], function () {
        Route::get('/', 'PatternController@index')->name('index');
    });

    // Download Page
    Route::group(['as' => 'downloads.', 'prefix' => 'downloads'], function () {
        Route::get('/', 'DownloadPageController@index')->name('index');
        Route::get('/create', 'DownloadPageController@create')->name('create');
        Route::post('/', 'DownloadPageController@store')->name('store');
        Route::get('/{download}/edit', 'DownloadPageController@edit')->name('edit');
        Route::put('/{download}/update', 'DownloadPageController@update')->name('update');
        Route::delete('/{download}', 'DownloadPageController@destroy')->name('destroy');
    });

    // Certificates and Standards Page
    Route::group(['as' => 'certs.', 'prefix' => 'certs'], function () {
        Route::get('/', 'CertificateController@index')->name('index');
        Route::get('/create', 'CertificateController@create')->name('create');
        Route::post('/', 'CertificateController@store')->name('store');
        Route::get('/{certificate}/edit', 'CertificateController@edit')->name('edit');
        Route::put('/{certificate}/update', 'CertificateController@update')->name('update');
        Route::delete('/{certificate}', 'CertificateController@destroy')->name('destroy');
    });

    // News
    Route::group(['as' => 'news.', 'prefix' => 'news'], function () {
        Route::get('/', 'NewsController@index')->name('index');
        Route::post('/', 'NewsController@store')->name('store');
        Route::get('/create', 'NewsController@create')->name('create');
        Route::get('/{schNew}/edit', 'NewsController@edit')->name('edit');
        Route::put('/{schNew}/update', 'NewsController@update')->name('update');
        Route::delete('/{schNew}', 'NewsController@destroy')->name('destroy');
        Route::delete('/images/{newsImage}', 'NewsController@deleteImage')->name('delete_image');
    });

    // Contents Management
    Route::group(['as' => 'contents.', 'prefix' => 'contents'], function () {
        Route::group(['as' => 'first_page.', 'prefix' => 'first-page', 'namespace' => 'Content'], function () {
            Route::group(['as' => 'slide_', 'prefix' => 'slides'], function () {
                Route::get('/', 'FirstPageController@slideAll')->name('index');
                Route::get('/create', 'FirstPageController@slideCreate')->name('create');
                Route::post('/', 'FirstPageController@slideStore')->name('store');
                Route::get('/{slide}/edit', 'FirstPageController@slideEdit')->name('edit');
                Route::delete('/{slide}/delete', 'FirstPageController@slideDestroy')->name('destroy');
                Route::put('/{slide}/update', 'FirstPageController@slideUpdate')->name('update');
                Route::put('/{slide}/visibility', 'FirstPageController@slideVisibility')->name('visibility');
            });

            Route::group([], function () {
                Route::get('/', 'FirstPageController@contentAll')->name('index');
                Route::get('/create', 'FirstPageController@contentCreate')->name('create');
                Route::post('/', 'FirstPageController@contentStore')->name('store');
                Route::get('/{content}/edit', 'FirstPageController@contentEdit')->name('edit');
                Route::put('/{content}/update', 'FirstPageController@contentUpdate')->name('update');
            });

            Route::group(['as' => 'youtube_', 'prefix' => 'youtube'], function () {
                Route::get('/', 'FirstPageController@youtubeIndex')->name('index');
                Route::put('/{youtube}/update', 'FirstPageController@youtubeUpdate')->name('update');
            });

            Route::group(['as' => 'project_ref_', 'prefix' => 'project-reference'], function () {
                Route::get('/', 'FirstPageController@projectRefIndex')->name('index');
                Route::get('/create', 'FirstPageController@projectRefCreate')->name('create');
                Route::get('/{projectReference}/edit', 'FirstPageController@projectRefEdit')->name('edit');
                Route::post('/', 'FirstPageController@projectRefStore')->name('store');
                Route::put('/{projectReference}/update', 'FirstPageController@projectRefUpdate')->name('update');
                Route::delete('/{projectReference}/delete', 'FirstPageController@projectRefDestroy')->name('destroy');
            });
        });

        Route::group(['prefix' => '/about-us', 'as' => 'about_us.'], function () {
            Route::get('/', 'ContentController@aboutUsIndex')->name('index');
            Route::put('/', 'ContentController@aboutUsUpdate')->name('update');
            Route::get('/youtube', 'ContentController@aboutUsYoutube')->name('youtube');
            Route::put('/youtube', 'ContentController@aboutUsYoutubeUpdate')->name('youtube_update');
        });

        Route::get('/{type}', 'ContentController@index')->name('index');
        Route::get('/create/{type}', 'ContentController@create')->name('create');
        Route::post('/store', 'ContentController@store')->name('store');
        Route::get('/{type}/{content}/edit', 'ContentController@edit')->name('edit');
        Route::delete('/{content}', 'ContentController@destroy')->name('destroy');
        Route::put('/{content}', 'ContentController@update')->name('update');
        Route::put('/visible/{content}', 'ContentController@handlingVisibility')->name('visible');
    });

    Route::group(['prefix' => 'distributors', 'as' => 'distributors.'], function () {
        Route::get('/', 'DistributorController@index')->name('index');
        Route::get('/create', 'DistributorController@create')->name('create');
        Route::get('/{authorized_distributor}/edit', 'DistributorController@edit')->name('edit');
        Route::post('/', 'DistributorController@store')->name('store');
        Route::put('/{authorized_distributor}', 'DistributorController@update')->name('update');
        Route::delete('/{authorized_distributor}', 'DistributorController@destroy')->name('destroy');
    });
});

