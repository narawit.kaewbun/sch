<?php

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () { //...
        Route::get('/', 'IndexPageController');
        Route::group(['prefix' => 'products_and_services'], function() {
            Route::get('/', 'ProductAndServiceController')->name('products_and_service.index');
            Route::get('/{name}', 'ProductAndServiceController')->name('products_and_service.index');
        });

        Route::get('/architectural_product/{name}', function ($name) {
            return view('front.architectura', ['name' => $name]);
        });

        Route::group(['prefix' => 'downloads', 'as' => 'downloads.'], function () {
            Route::get('/', 'DownloadPageController@Index')->name('index');
        });

        Route::group(['prefix' => 'projects', 'as' => 'projects.'], function () {
            Route::get('/', 'ProjectController@index')->name('index');
            Route::get('/{project}', 'ProjectController@show')->name('show');
        });

        Route::group(['prefix' => 'news', 'as' => 'news.'], function() {
            Route::get('/', 'NewsController@index')->name('index');
            Route::get('/{sch_new}', 'NewsController@show')->name('show');
        });

        Route::group(['prefix' => 'contact', 'as' => 'contact.'], function() {
            Route::get('/', 'ContactController@index')->name('index');
            Route::post('/', 'ContactController@submitContactForm')->name('submit_form');
        });

        Route::get('/authorized', 'DistributorController')->name('authorized');

        Route::get('/about', 'AboutUsController')->name('about');
    }
);
