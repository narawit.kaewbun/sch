<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    private $user;
    private $baseRoute = 'admin.auth.login';

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['username' => 'admin1']);
    }

    /**
     * @test
     */
    public function a_user_can_login()
    {
        $response = $this->post(route($this->baseRoute), ['username' => $this->user->username, 'password' => '123456']);

        $response->assertRedirect('/admin');
    }

    /**
     * @test
     */
    public function a_user_can_logout()
    {
        $response = $this->post(route('admin.auth.logout'));

        $response->assertRedirect('/admin');
    }

    /**
     * @test
     */
    public function a_username_and_password_must_be_fill()
    {
        $response = $this->post(route($this->baseRoute), ['username' => '', 'password' => '']);

        $response->assertSessionHasErrorsIn('default', ['username', 'password']);
    }

    /**
     * @test
     */
    public function a_user_cannot_login_if_username_or_password_is_invalid()
    {
        $response = $this->post(route($this->baseRoute), ['username' => 'admin1', 'password' => '12345']);

        $response->assertSessionHasErrorsIn('default', ['auth_failed']);
    }
}
