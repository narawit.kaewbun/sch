<?php

return [
    'all' => [
        'A' => explode(',', env('ALL_A')),
        'B' => explode(',', env('ALL_B')),
        'C' => explode(',', env('ALL_C')),
        'D' => explode(',', env('ALL_D')),
    ],
    'th' => [
        'A' => explode(',', env('TH_A')),
        'B' => explode(',', env('TH_B')),
        'C' => explode(',', env('TH_C')),
        'D' => explode(',', env('TH_D')),
    ],
    'inter' => [
        'A' => explode(',', env('INTER_A')),
        'B' => explode(',', env('INTER_B')),
        'C' => explode(',', env('INTER_C')),
        'D' => explode(',', env('INTER_D')),
    ]
];
