@extends('layouts.front')
@section('content')
<section class="architectura-header">
    <img src="{{ asset('images/i_bs_head.jpg') }}" class="img-fluid">
</section>
<section class="architectura-building py-5" id="building_system">
    <div class="container">
        <h3><span class="font-weight-bold">SMS</span>&nbsp;BUILDING SYSTEMS</h3>
        <p>
            SMS BUILDING SYSTEMS offers an extensive range of Australian engineered and tested commercial and semi-commercial window and door solutions specifically designed for the Thai construction industry. Close attention has been given to developing products using quality hardware and design concepts that offer enhanced weather resistance, improved acoustic and thermal performances, and flexible installation concepts.
        </p>
        <p class="mt-3">
            The SMS BUILDING SYSTEMS was designed with ease of manufacturing in mind to provide better performing products without the high cost. Our building products are suitable for all types of applications from housing and low to medium rise condominiums to commercial buildings and can be optioned up for high performance applications. Designers and property owners will be delighted by the diversity of designs available. Our multi-frame compatible system allows different glazing solutions to be integrated to provide the utmost in design flexibility.
        </p>
        <div class="d-flex flex-column flex-sm-row justify-content-between">
            <div>
                <img src="{{ asset('images/i_bs_smsbs_left.jpg') }}" class="img-fluid">
            </div>
            <div>
                <img src="{{ asset('images/i_bs_smsbs_right.jpg') }}" class="img-fluid">
            </div>
        </div>
        <div class="mt-3 download">
            <a href="/" target="_blank" class="font-weight-bold"><img src="{{ asset('images/g_pdf_icon.png') }}" class="img-fluid">&nbsp;DOWNLOAD CATALOGUE</a>
        </div>
        <div class="mt-5 quality">
            <h3 class="font-weight-bold">QUALITY BY DESIGN</h3>
            <div class="row">
                <div class="col-12 col-sm-6 mt-3">
                    <p class="font-weight-bold">HOMELIFE</p>
                    <p>Residential Windows & Doors</p>
                    <img src="{{ asset('images/i_bs_homelife.jpg') }}" class="img-fluid">
                    <p class="float-right">"FOR EVERYDAY LIVING"</p>
                </div>
                <div class="col-12 col-sm-6 mt-3">
                    <p class="font-weight-bold">NEXTGEN</p>
                    <p>High Rise Window & Door Systems</p>
                    <img src="{{ asset('images/i_bs_nextgen.jpg') }}" class="img-fluid">
                    <p class="float-right">"INNOVATION BY DESIGN"</p>
                </div>
                <div class="col-12 col-sm-6 mt-3">
                    <p class="font-weight-bold">PROLINE</p>
                    <p>Commercial Framing Systems</p>
                    <img src="{{ asset('images/i_bs_proline.jpg') }}" class="img-fluid">
                    <p class="float-right">"OPEN A WORLD OF POSSIBILITIES"</p>
                </div>
                <div class="col-12 col-sm-6 mt-3">
                    <p class="font-weight-bold">SIGNATURE</p>
                    <p>High Performance Windows & Doors</p>
                    <img src="{{ asset('images/i_bs_singature.jpg') }}" class="img-fluid">
                    <p class="float-right">"BEYOND IMAGINATION"</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="hardware py-3" id="hardware">
    <div class="container">
        <h3 class="font-weight-bold">HARDWARE</h3>
        <p>To complement our building systems product range SMS have sourced and developed a comprehensive range of hardware and accessories specifically for use with SMS Building Systems products. Close attention has been given in the selection of materials and suppliers to ensure our hardware and accessories offer improved security, durability and are aesthetically pleasing.</p>
        <img src="{{ asset('images/i_bs_hardware.jpg') }}" class="img-fluid">
    </div>
</section>
<section class="research pt-3 pb-5" id="research">
    <div class="container">
        <h3 class="font-weight-bold">RESEARCH & DEVELOPMENT</h3>
        <p>Our technical services and research team provides customised design solutions to suit individual company requirements. Our goal is to leverage our research and development capabilities to generate distinctive value for our customers through innovative design solutions that deliver improved performance, durability and employing processing techniques that reduce cost.</p>
        <img src="{{ asset('images/i_bs_research.jpg') }}" class="img-fluid">
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    var name = '{{ $name }}';
    $(document).ready(function() {
        console.log($('#name').html($(this).text()));
        $('html, body').animate({
            scrollTop: $("#" + name).offset().top
        }, 2000);
    });
</script>
@endsection