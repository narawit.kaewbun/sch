@extends('layouts.front')
@section('content')
<section class="contact-header">
    <img src="{{ asset('images/i_h3.jpg') }}" alt="" class="img-fluid w-100">
</section>
<section class="contact-content py-5">
    <div class="container">
        <div class="text-center">
            <h3 class="title">CONTACT US</h3>
            <span class="title-line"></span>
        </div>
        @foreach($errors->all() as $key => $value)
            <div class="text-center text-danger">
                {{ $value }}
            </div>
        @endforeach
        @if(session('success'))
            <div class="text-center text-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <form method="POST" action="{{ route('contact.submit_form') }}" class="mt-5">
            @csrf
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="NAME">
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="email" class="form-control" name="email" placeholder="EMAIL">
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="tel" placeholder="TEL">
                </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="subject" placeholder="SUBJECT">
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="3" name="message" placeholder="MESSAGE"></textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-submit">SUBMIT</button>
        </form>
        <div class="row mt-3">
            <div class="col-12 col-sm-6">
                <h3><b>OFFICE</b></h3>
                <p>บริษัท ซิมเมอร์ เมตัล สแตนดาร์ด จำกัด</p>
                <p>1070/4-6 ถนนพหลโยธิน ลาดพร้าว จตุจักร กรุงเทพมหานคร 10900</p>
                <p>โทร: 662-617-6006</p>
                <p>แฟกซ์: 662-272-1546</p>
                <a href="mailto:info@schimmermetal.co.th?Subject=Hello%20again" target="_top">info@schimmermetal.co.th</a>
            </div>
            <div class="col-12 col-sm-6">
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3874.492686298192!2d100.558054!3d13.809428!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1582987506816!5m2!1sen!2sus"></iframe>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-sm-6">
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.google.co.th/maps/dir/13.8132581,100.5690331/13.8117621,100.5621627/@13.8080071,100.5667157,16z/data=!4m2!4m1!3e0"></iframe>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <h3><b>FACTORY</b></h3>
                <p>Schimmer Metal Standard Company Limited (Factory)</p>
                <p>1/4 Moo 7 Tambon Banthew, Ampur Sena, Ayutthaya 13110</p>
                <p>Tel: 663-537-5080-3</p>
                <p>Fax: 663-537-5084</p>
                <a href="mailto:info@schimmermetal.co.th?Subject=Hello%20again" target="_top">info@schimmermetal.co.th</a>
            </div>
        </div>

        <!-- <div class="distributor mt-5 p-3 text-center">
            <a href="{{ asset('images/file_download/SMS_Building_Systems_Authorized_Distributor.pdf') }}" target="_blank">
                <h4>SMS Building Systems - Authorized Distributor <i class="fas fa-download"></i></h4>
            </a>
        </div> -->

    </div>
</section>
@endsection()
