@extends('layouts.front')
@section('content')
<section class="contact-header">
    <img src="{{ asset('images/i_h3.jpg') }}" alt="" class="img-fluid w-100">
</section>
<section class="about py-5">
    <div class="container">
        <h3 class="title">ABOUT US</h3>
        <span class="title-line"></span>
        <p class="mt-3">
            Schimmer Metal Standard is Thailand’s most trusted name in the supply of aluminium extrusion profiles. Established in 1992, our manufacturing operations are in Ayutthaya province approximately 90 kilometres north of Bangkok, with the head office based in metropolitan Bangkok. Our world-class facilities are fully equipped in all facets of the extrusion process, including in-house die production, thermally broken extrusions, precision machining, anodising, powder coating, and fluorocarbon and woodgrain finishing. SMS Schimmer services a broad spectrum of industry groups including architectural, construction, automotive, industrial, furniture and electronics. With more than 25 years of experience in Thailand and international markets, SMS Schimmer has developed a reputation of going beyond expectation by delivering our customers only the highest quality goods and services.
        </p>
        <div class="vdo mt-5">
            <div class="title text-center">
                <h3>25 YEARS OF EXPERIENCE</h3>
                <p>25 YEARS OF EXCELLENCE</p>
            </div>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YRVrVhZaQXY"></iframe>
            </div>
        </div>
        <div class="mt-5">
            <h3 class="title">CERTIFICATE/STANDARD</h3>
            <span class="title-line"></span>
            <div class="mt-3 d-flex flex-wrap justify-content-start">
                @foreach($data['certificate'] as $items)
                <div class="p-2">
                    <img src="{{ asset($items->image) }}" class="img-fluid" width="250">
                </div>
                @endforeach
            </div>
        </div>
        <hr>
        <div class="mt-5">
            <div class="mt-3 d-flex flex-wrap justify-content-start">
                @foreach($data['standard'] as $items)
                <div class="p-2">
                    <img src="{{ asset($items->image) }}" class="img-fluid" width="145" height="90">
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection