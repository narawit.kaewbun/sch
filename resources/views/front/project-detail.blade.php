@extends('layouts.front')
@section('styles')
<style>

</style>
@endsection()
@section('content')
<section class="projects">
    <div class="container">
        <div class="controls d-flex justify-content-between">
            <div class="d-flex justify-content-start">
                <a href="{{ route('projects.show', $previous == null ? '' : $previous ) }}" class="d-{{ $previous == null ? 'none' : 'block' }}"><i class="fas fa-chevron-left fa-2x"></i></a>
            </div>
            <div class="d-flex justify-content-end">
                <a href="{{ route('projects.show', $next == null ? '' : $next ) }}" class="d-{{ $next == null ? 'none' : 'block' }} "><i class="fas fa-chevron-right fa-2x"></i></a>
            </div>
        </div>
    </div>
    <div class="projectsMenu d-flex justify-content-between align-items-center">
        <p class="float-left">
            <a href="{{ route('projects.index', ['type' => 'th']) }}">
                THAILAND
            </a>
            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
            <a href="{{ route('projects.index', ['type' => 'inter']) }}">
                INTERNATIONAL
            </a>
        </p>
        <p class="float-right">
            <a href="{{ route('projects.index') }}">
                VIEW ALL
            </a>
        </p>
    </div>
</section>
<section class="project-detail">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 px-0">
                <img class="img-fluid w-100" src="{{ asset($project->content_image) }}" alt="">
            </div>
            <div class="col-12 col-sm-6 py-3">
                <div class="container">
                    <h3 class="title">{{ $project->name }}</h3>
                    <p>{{ $project->location }}</p>

                    @php
                    $data = json_decode($project->data)
                    @endphp


                    @if($project->data)
                    @foreach($data as $key => $value)
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <p class="font-weight-bold">{{ isset($value[2]) ? $value[2] : $value[0] }}</p>
                        </div>
                        <div class="col-12 col-sm-6">
                            <p>{{ isset($value[2]) ? $value[2] : $value[1] }}</p>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
