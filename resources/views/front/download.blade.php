@extends('layouts.front')
@section('content')
<section class="download-page py-5">
    <div class="container">
        <h3 class="title-text">DOWNLOAD</h3>
        <span class="title-line"></span>
        <div class="row mt-3">
            @foreach($downloads as $key => $download)

            @php
            $times = 0
            @endphp

            @if ($key === 0)
                @php
                    $times = 0
                @endphp
            @else
                @php
                    $times = ($key * 0.25)
                @endphp
            @endif

            <div class="col-12 col-sm-3 mt-3">
                <div class="wow fadeIn animated text-center text-sm-left" data-wow-delay="{{ $times }}s">
                    <a href="{{ asset($download->file) }}" target="_blank">
                        <img src="{{ asset($download->cover) }}" alt="" width="200" class="img-fluid block-shadow">
                        <div class="text-download mt-3">
                            <p>{{ $download->title }}</p>
                            <p>{{ $download->sub_title }}</p>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
           
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        new WOW().init();
    });
</script>
@endsection