@extends('layouts.front')
@section('title')
SMS : Aluminium Extrusion in Thailand.
@endsection
@section('styles')
@endsection
@section('content')
<section class="slide-cover">
    <div id="sldecover" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($slides as $key => $item)
            <li data-target="#sldecover" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach($slides as $key => $item)
            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                <img src="{{ asset($item->image) }}" class="d-block w-100">
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#sldecover" role="button" data-slide="prev">
            <span class="d-none d-sm-block">
                <i class="fas fa-chevron-left fa-3x"></i>
            </span>
            <span class="d-block d-sm-none">
                <i class="fas fa-chevron-left"></i>
            </span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#sldecover" role="button" data-slide="next">
            <span class="d-none d-sm-block">
                <i class="fas fa-chevron-right fa-3x"></i>
            </span>
            <span class="d-block d-sm-none">
                <i class="fas fa-chevron-right"></i>
            </span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<section class="extrusion py-5">
    <div class="container">
        <div class="row">
            @foreach($contents as $key => $item)
            <div class="col-12 col-sm-6 wow animated fadeIn" data-wow-delay="{{ $key*0.5 }}s">
                <div class="title">
                    <h3><a href="{{ $item->link }}" class="font-weight-bold">{{ $item->en_title }}</a></h3>
                    <span class="title-line"></span>
                </div>
                <div class="content mt-3">
                    <p>
                        {{ $item->content }}
                    </p>
                    <a href="{{ $item->link }}">
                        <img src="{{ asset($item->image) }}" class="img-fluid">
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section class="vdo py-5">
    <div class="container">
        <div class="box-content p-3 p-sm-5">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9 wow fadeInLeft" data-wow-delay="0.5s">
                        <iframe class="embed-responsive-item" src="{{ $youtube->link }}"></iframe>
                    </div>
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0 d-flex justify-content-center align-items-center">
                    <div class="wow fadeInRight" data-wow-delay="0.75s">
                        <h2>
                            <span class="font-weight-bold">SMS SCHIMMER</span><br />
                            BEYOND EXPECTATION
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="project-ref py-3">
    <div class="container">
        <div class="text-center">
            <h3 class="title font-weight-bold wow fadeInUp" data-wow-delay="0.5s">PROJECT REFERENCE</h3>
            <p class="wow fadeInUp" data-wow-delay="0.75s">
                WE ARE A TRUSTED SUPPLIER OF MANY HIGH-PROFILE PROJECTS <br>
                IN THAILAND AND AROUND THE WORLD.
            </p>
        </div>
        <div class="mt-3">
            <div class="row wow fadeInUp" data-wow-delay="0.75s">
                @foreach($projectRefs as $key => $items)
                <div class="col-12 col-sm-6  {{ $key%2 == 0 ? 'pr' : 'pl' }}-sm-0">
                    <div class="img-hover-zoom img-hover-zoom--basic">
                        <a href="{{ route('projects.show', $items['id']) }}">
                            <img src="{{ asset($items['image']) }}" class="img-fluid d-md-none d-block">
                            <div class="home-projects d-md-block d-none" style="background-image: url( {{ $items['image'] }} )"></div>
                        </a>
                        <div class="project-text">
                            <h5>{{ $items['name'] }}</h5>
                            <p>{{ $items['location'] }}</p>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="box-control py-5 text-center wow fadeIn" data-wow-delay="0.5s">
                <a href="{{ route('projects.index') }}" class="btn-submit">Any enquiry ?</a>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        new WOW().init();
    });
</script>
@endsection
