@extends('layouts.front')
@section('content')
<section class="products-header">
    <img src="{{ asset('images/i_extrusion_head_2.jpg') }}" class="img-fluid">
</section>
<section class="products-content py-5">
    <div class="container">
        <h3 class="title font-weight-bold">EXTRUSION</h3>
        <span class="title-line"></span>
        @foreach($data['extrusion'] as $key => $item)

        <div class="row mt-3 mt-sm-5" id="{{ $key }}">
            <div class="col-12 col-sm-6">
                <img src="{{ asset($item->image) }}" class="img-fluid">
            </div>
            <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                <h5 class="title font-weight-bold">{{ $item->title }}</h5>
                <p>
                    {{ $item->content }}
                </p>
            </div>
        </div>

        @endforeach

        <div class="mt-3 mt-sm-5">
            <h3 class="title font-weight-bold">FINISHING</h3>
            <span class="title-line"></span>
            @foreach($data['finishing'] as $key => $item)

            <div class="row mt-3 mt-sm-5" id="{{ $key }}">
                <div class="col-12 col-sm-6">
                    <img src="{{ asset($item->image) }}" class="img-fluid">
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                    <h5 class="title font-weight-bold">{{ $item->title }}</h5>
                    <p>
                        {{ $item->content }}
                    </p>
                </div>
            </div>

            @endforeach
        </div>

        <div class="mt-3 mt-sm-5">
            <h3 class="title font-weight-bold">EXTENDED SERVICES</h3>
            <span class="title-line"></span>
            @foreach($data['extended-service'] as $key => $item)

            <div class="row mt-3 mt-sm-5" id="{{ $key }}">
                <div class="col-12 col-sm-6">
                    <img src="{{ asset($item->image) }}" class="img-fluid">
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                    <h5 class="title font-weight-bold">{{ $item->title }}</h5>
                    <p>
                        {{ $item->content }}
                    </p>
                </div>
            </div>

            @endforeach
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    var decodeHTML = function (html) {
        var txt = document.createElement('textarea');
        txt.innerHTML = html;
        return CSS.escape(txt.value);
    };

    var name = '{{ $name }}';
    var decodeName = decodeHTML(name);

    $(document).ready(function() {
        $('html, body').animate({
            scrollTop: $(`#${decodeName}`).offset().top
        }, 2000);
    });
</script>
@endsection
