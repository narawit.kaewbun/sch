@extends('layouts.front')
@section('styles')
@endsection()
@section('content')
<section class="news py-3">
    <div class="container">
        <h3 class="title">NEWS</h3>
        <span class="title-line"></span>
        <div class="mt-5">
            @foreach($news as $key => $items)
            <div class="mt-5 wow fadeIn animated" data-wow-delay="{{ $key*0.5 }}s">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <a href="{{ route('news.show', $items->id) }}">
                            <img class="img-fluid w-100" src="{{ $items->newImages[0]->image }}" alt="">
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 mt-3 mt-sm-0">
                        <a href="{{ route('news.show', $items->id) }}">
                            <p>{{ $items->en_title }}</p>
                            <p>{{ date('d M Y', strtotime($items->updated_at)) }}</p>
                        </a>
                    </div>
                </div>
                <hr>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        new WOW().init();
    });
</script>
@endsection