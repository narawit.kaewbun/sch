@extends('layouts.front')
@section('styles')
<style>

</style>
@endsection()
@section('content')
<section class="projects">
    <div class="projectsMenu d-flex justify-content-between align-items-center">
        <p class="float-left">
            <a href="{{ route('projects.index', ['type' => 'th']) }}">
                THAILAND
            </a>
            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
            <a href="{{ route('projects.index', ['type' => 'inter']) }}">
                INTERNATIONAL
            </a>
        </p>
        <p class="float-right">
            <a href="{{ route('projects.index') }}">
                VIEW ALL
            </a>
        </p>
    </div>
</section>
<section class="projects-view">

    <!-- type-a -->
    <div id="type-a">
        @foreach($data['A'] as $key => $items)
        <div class="item-{{ $items['position'] }} position-relative">
            <div class="img-hover-zoom img-hover-zoom--basic">
                <a href="{{ route('projects.show', [$items['id']]) }}">
                    <img src="{{ asset($items['cover_image']) }}" class="img-fluid">
                </a>
                <div class="project-text">
                    <h5>{{ $items['name'] }}</h5>
                    <p>{{ $items['location'] }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- type-b -->
    <div id="type-b">
        @foreach($data['B'] as $key => $items)
        <div class="item-{{ $items['position'] }} position-relative">
            <div class="img-hover-zoom img-hover-zoom--basic">
                <a href="{{ route('projects.show', [$items['id']]) }}">
                    <img src="{{ asset($items['image']) }}" class="img-fluid">
                </a>
                <div class="project-text">
                    <h5>{{ $items['name'] }}</h5>
                    <p>{{ $items['location'] }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- type-c -->
    <div id="type-c">
        @foreach($data['C'] as $key => $items)
        <div class="item-{{ $items['position'] }} position-relative">
            <div class="img-hover-zoom img-hover-zoom--basic">
                <a href="{{ route('projects.show', [$items['id']]) }}">
                    <img src="{{ asset($items['image']) }}" class="img-fluid">
                </a>
                <div class="project-text">
                    <h5>{{ $items['name'] }}</h5>
                    <p>{{ $items['location'] }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- type-d -->
    <div id="type-d">
        @foreach($data['D'] as $key => $items)
        <div class="item-{{ $items['position'] }} position-relative">
            <div class="img-hover-zoom img-hover-zoom--basic">
                <a href="{{ route('projects.show', [$items['id']]) }}">
                    <img src="{{ asset($items['image']) }}" class="img-fluid">
                </a>
                <div class="project-text">
                    <h5>{{ $items['name'] }}</h5>
                    <p>{{ $items['location'] }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="projects-footer py-5 text-center">
        <a href="{{ route('contact.index') }}" class="btn btn-enquiry">Any enquiry?</a>
    </div>
</section>
@endsection
