@extends('layouts.front')
@section('content')
<section class="contact-header">
    <img src="{{ asset('images/i_h3.jpg') }}" alt="" class="img-fluid w-100">
</section>
<section class="contact-content py-5">
    <div class="container">
        <div class="text-center">
            <h3 class="title">AUTHORIZED DISTRIBUTOR</h3>
            <span class="title-line"></span>
        </div>
        @foreach($distributors as $items)
        <div class="row mt-5">
            <div class="col-12 col-sm-6">
                <h3><b>{{ $items->type }}</b></h3>
                <h2><b>{{ $items->company_name }}</b></h2>
                <p>{{ $items->address }}</p>
                <p>โทร: {{ $items->phone }}</p>
                <p>แฟกซ์: {{ $items->fax }}</p>
                <a href="mailto:{{ $items->email }}?Subject=Hello%20again" target="_top">{{ $items->email }}</a>
            </div>
            <div class="col-12 col-sm-6">
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="{{ $items->google_map_embed_url }}"></iframe>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
@endsection()