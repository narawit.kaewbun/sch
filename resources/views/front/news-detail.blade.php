@extends('layouts.front')
@section('styles')
<link href="{{ asset('css/ninja-slider.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/thumbnail-slider.css') }}" rel="stylesheet" type="text/css" />
<style>
    a {
        color: #1155CC;
    }

    ul li {
        padding: 10px 0;
    }

    code {
        display: block;
        white-space: pre;
        background-color: #f6f6f6;
        padding: 8px;
        overflow: auto;
        border: 1px dotted #999;
        margin: 6px 0;
    }

    #ninja-slider {
        color: transparent;
    }
</style>
@endsection()
@section('content')
<section class="news-detail">
    <div class="container">
        <!--start-->
        <div id='ninja-slider'>
            <div>
                <div class="slider-inner">
                    <ul>
                        @foreach($schNew->newImages as $items)
                            <li><a class="ns-img" href="{{ asset($items->image) }}"></a></li>
                        @endforeach
                    </ul>
                    <div class="fs-icon" title="Expand/Close"></div>
                </div>
                <div id="thumbnail-slider">
                    <div class="inner">
                        <ul>
                            @foreach($schNew->newImages as $key => $items)
                            <li>
                                <a class="thumb" href="{{ asset($items->image) }}"></a>
                                <span>{{ $key }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="contet mt-5">
            <h3>{{ $schNew->en_title }}</h3>
            <p>{{ date('d M Y', strtotime($schNew->updated_at)) }}</p>
            <p>
                {!! $schNew->en_content !!}
            </p>
        </div>
        <div class="page-controls d-flex justify-content-between">
            <div class="d-flex justify-content-start">
                <a href="{{ route('news.show', $previous == null ? '' : $previous) }}" class="d-{{ $previous == null ? 'none' : 'blcok' }} btn btn-event">Previous</a>
            </div>
            <div class="d-flex justify-content-center">
                <a href="{{ route('news.index') }}" class="btn btn-event">All News</a>
            </div>
            <div class="d-flex justify-content-end">
                <a href="{{ route('news.show', $next == null ? '' : $next) }}" class="d-{{ $next == null ? 'none' : 'blcok' }} btn btn-event">Next</a>
            </div>
        </div>
    </div>
</section>
@endsection()
@section('scripts')
<script src="{{ asset('js/ninja-slider.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/thumbnail-slider.js') }}" type="text/javascript"></script>
<script>
</script>
@endsection()