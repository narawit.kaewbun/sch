<div style="display: flex; flex-direction: column;">
    <span>Name: {{ $data['name'] }}</span>
    <span>Email: {{ $data['email'] }}</span>
    <span>Tel: {{ $data['tel'] }}</span>
    <span>Subject: {{ $data['subject'] }}</span>
    <span>Message: {{ $data['message'] }}</span>
</div>
