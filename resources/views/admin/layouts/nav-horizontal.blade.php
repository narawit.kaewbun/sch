<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>@lang('admin/menu.project_management')
                        <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('admin.project_management.index', 'all') }}">@lang('admin/menu.all_project')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.project_management.index', 'th') }}">@lang('admin/menu.th_project')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.project_management.index', 'inter') }}">@lang('admin/menu.inter_project')</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#"> <i class="fe-file-text"></i>@lang('admin/menu.download_management')
                        <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('admin.downloads.index') }}">@lang('admin/menu.all')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.downloads.create') }}">@lang('admin/menu.download_management_create')</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-layers"></i>@lang('admin/menu.content_management')
                        <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li class="has-submenu">
                            <a href="#">@lang('admin/menu.first_page')
                                <div class="arrow-down"></div>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{ route('admin.contents.first_page.slide_index') }}">@lang('admin/menu.slide')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.first_page.index') }}">@lang('admin/menu.first_page_content')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.first_page.youtube_index') }}">@lang('admin/menu.youtube_link_setup')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.first_page.project_ref_index') }}">@lang('admin/menu.project_reference')</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="javascript:void(0)">
                                @lang('admin/menu.about_us')
                                <div class="arrow-down"></div>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{ route('admin.contents.about_us.index') }}">@lang('admin/menu.content')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.about_us.youtube') }}">@lang('admin/menu.youtube')</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">@lang('admin/menu.extrusion')
                                <div class="arrow-down"></div>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{ route('admin.contents.index', ['extrusion']) }}">@lang('admin/menu.all')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.create', ['extrusion']) }}">@lang('admin/menu.create')</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">@lang('admin/menu.finishing')
                                <div class="arrow-down"></div>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{ route('admin.contents.index', ['finishing']) }}">@lang('admin/menu.all')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.create', ['finishing']) }}">@lang('admin/menu.create')</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">@lang('admin/menu.extended_service')
                                <div class="arrow-down"></div>
                            </a>
                            <ul class="submenu">
                                <li>
                                    <a href="{{ route('admin.contents.index', ['extended-service']) }}">@lang('admin/menu.all')</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.contents.create', ['extended-service']) }}">@lang('admin/menu.create')</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-layers"></i>@lang('admin/menu.cert_and_standard')
                        <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('admin.certs.index') }}">@lang('admin/menu.all')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.certs.create') }}">@lang('admin/menu.upload')</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-layers"></i>@lang('admin/menu.news')
                        <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('admin.news.index') }}">@lang('admin/menu.all')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.news.create') }}">@lang('admin/menu.add_news')</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-layers"></i>@lang('admin/menu.distributor_management')
                        <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('admin.distributors.index') }}">@lang('admin/menu.all')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.distributors.create') }}">@lang('admin/menu.distributor_management_create')</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->
