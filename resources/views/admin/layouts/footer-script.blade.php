<!-- Vendor js -->
<script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

@yield('script')

<!-- App js -->
<script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const extraFieldContainer = $('#extra-field-container');
    const extraPreviewContainer = $('#extra-preview-container');

    let extraFieldCount = 0;
    let positionOptions = $('#position');


    function addField() {
        let extraFieldId = `extra-field-${extraFieldCount}`;
        let extraFieldName = `extra_field_${extraFieldCount}`;

        let extraValueId = `extra-value-${extraFieldCount}`;
        let extraValueName = `extra_value_${extraFieldCount}`;

        let extraFieldCover = `extra-field-cover-${extraFieldCount}`;

        let extraPreviewField = `extra-field-${extraFieldCount}-preview`;
        let extraPreviewValue = `extra-value-${extraFieldCount}-preview`;
        let extraPreviewCover = `extra-preview-cover-${extraFieldCount}`;

        let extraFieldHTML = `
            <div class="form-group d-flex align-items-md-start flex-column flex-md-row" id="${extraFieldCover}">
                <div class="form-row flex-grow-1">
                    <div class="form-group col-md-6 align-items-start">
                        <input type="text"
                               class="form-control"
                               id="${extraFieldId}"
                               name="${extraFieldName}"
                               placeholder="@lang('admin/placeholder.extra_field')"
                               oninput="onCreateProjectInputChange(this)"
                               required>
                    </div>
                    <div class="form-group col-md-6 align-items-start">
                        <input type="text"
                               class="form-control"
                               id="${extraValueId}"
                               name="${extraValueName}"
                               placeholder="@lang('admin/placeholder.extra_value')"
                               oninput="onCreateProjectInputChange(this)"
                               required>
                    </div>
                </div>
                <button type="button" class="btn btn-danger ml-md-2" data-extra-cover-id="${extraFieldCount}" onclick="deleteField(this)">@lang('admin/button.delete')</button>
            </div>`;

        let extraPreviewHTML = `
                <div class="row mb-2" id="${extraPreviewCover}">
                    <div class="col-md-6 font-weight-bold text-capitalize" id="${extraPreviewField}">Label</div>
                    <div class="col-md-6 text-capitalize" id="${extraPreviewValue}">Description</div>
                </div>`;

        extraFieldContainer.append(extraFieldHTML);
        extraPreviewContainer.append(extraPreviewHTML);
        extraFieldCount++;
    }

    function deleteField(e) {
        const coverId = $(e).data('extra-cover-id');

        $(`#extra-field-cover-${coverId}`).remove();
        $(`#extra-preview-cover-${coverId}`).remove();
    }

    function getPositionsViaPattern(e = null, type, position = null, isInUsed = false) {
        let pattern;

        if (e) {
            pattern = $(e).val();
        } else {
            pattern = $('#pattern').val();
        }

        let route = '{{ route('admin.patterns.index') }}';

        route += `?pattern=${pattern}&type=${type}&is_in_used=${isInUsed}`;

        $.ajax({
            type: 'GET',
            url: route,
            success: function (data) {
                if (Object.keys(data).length !== 0) {
                    positionOptions.empty();

                    for (let key of Object.keys(data)) {
                        positionOptions.append(`<option value=${data[key]}>${data[key]}</option>`);
                    }

                    $('#position > *').each(function(key, ele) {
                        if (position === $(ele).val()) {
                            $(ele).attr('selected', 'selected');
                        }
                    });
                } else {
                    positionOptions.empty();
                    positionOptions.append(`<option value=''>@lang('admin/placeholder.project_position')</option>`);
                }
            },
            error: function (err) {
                alert(err);
            }
        })
    }

    function onCreateProjectInputChange(e) {
        if (e.name === 'image') {
            setPreviewImage(e);
        } else {
            $(`#${e.id}-preview`).text(e.value);
        }
    }

    function setPreviewImage(e) {
        const reader = new FileReader();

        reader.onloadend = function () {
            $('.image-preview').each((index, item) => {
                $(item).attr('src',reader.result);
            });
        };

        if (e.files[0]) {
            reader.readAsDataURL(e.files[0]);
        } else {
            preview = '';
        }
    }
</script>
@yield('script-bottom')
