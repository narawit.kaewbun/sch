<!-- Topbar Start -->
<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">

            <li class="dropdown notification-list">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle nav-link">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#"
                   role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{ asset('assets/images/users/user-1.jpg') }}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ml-1">
                        {{ Auth::user()->username }} <i class="mdi mdi-chevron-down"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <a href="{{ route('admin.profile.index') }}" class="dropdown-item notify-item">
                        <i class="fe-settings"></i>
                        <span>Settings</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <!-- item-->
                    <form action="{{ route('admin.auth.logout') }}" method="POST">
                        @csrf
                        <button class="dropdown-item notify-item" type="submit">
                            <i class="fe-log-out"></i>
                            <span>Logout</span>
                        </button>
                    </form>
                </div>
            </li>
        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="{{ route('admin.index') }}" class="logo text-center">
                <span class="logo-lg">
                    <img src="{{ asset('images/logo_sms.gif') }}" alt="" height="40">
                    <!-- <span class="logo-lg-text-light">UBold</span> -->
                </span>
                <span class="logo-sm">
                    <!-- <span class="logo-sm-text-dark">U</span> -->
                    <img src="{{ asset('images/logo_sms.gif') }}" alt="" height="30">
                </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

            @role('super-admin')
            <li class="dropdown d-none d-lg-block">
                <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    @lang('admin/menu.admin_management')
                    <i class="mdi mdi-chevron-down"></i>
                </a>
                <div class="dropdown-menu">
                    <!-- item-->
                    <a href="{{ route('admin.admin_management.index') }}" class="dropdown-item">
                        <span>@lang('admin/menu.all')</span>
                    </a>

                    <!-- item-->
                    <a href="{{ route('admin.admin_management.create') }}" class="dropdown-item">
                        <span>@lang('admin/menu.admin_management_create')</span>
                    </a>
                </div>
            </li>
            @endrole
        </ul>
    </div> <!-- end container-fluid-->
</div>
<!-- end Topbar -->
