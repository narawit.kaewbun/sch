@yield('css')

<!-- App css -->
<link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css"/>

<!-- Custom css -->
<link href="{{ URL::asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- Sweet Alert-->
<link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>

<!-- Sweet Alerts js -->
<script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
