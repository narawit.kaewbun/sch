@extends('admin.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin/menu.profile')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.profile')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <div class="card-box">
                                <div class="d-flex flex-column align-items-center">
                                    <img src="{{ asset('assets/images/Users/user-1.jpg') }}"
                                         class="rounded-circle avatar-lg img-thumbnail"
                                         alt="profile-image">

                                    <h4 class="mb-0">{{ Auth::user()->email }}</h4>
                                    <p class="text-muted">{{ "@" . Auth::user()->username }}</p>
                                </div>

                                <form action="{{ route('admin.profile.change_password') }}"
                                      method="POST"
                                      autocomplete="off"
                                      class="parsley-examples">
                                    @csrf
                                    <h5 class="mb-4 text-uppercase"><i
                                            class="mdi mdi-shield-lock-outline mr-1"></i>@lang('admin/label.change_password')
                                    </h5>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label
                                                    for="current_password">@lang('admin/label.current_password')</label>
                                                <input type="password"
                                                       class="form-control"
                                                       id="current_password"
                                                       name="current_password"
                                                       placeholder="@lang('admin/placeholder.current_password')"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label for="new_password">@lang('admin/label.new_password')</label>
                                                <input type="password"
                                                       class="form-control"
                                                       id="new_password"
                                                       name="new_password"
                                                       placeholder="@lang('admin/placeholder.new_password')"
                                                       required>
                                            </div>
                                            <div class="form-group">
                                                <label
                                                    for="new_password_confirmation">@lang('admin/label.new_password_confirmation')</label>
                                                <input type="password"
                                                       class="form-control"
                                                       id="new_password_confirmation"
                                                       name="new_password_confirmation"
                                                       placeholder="@lang('admin/placeholder.new_password_confirmation')"
                                                       required>
                                            </div>
                                            <button type="submit"
                                                    class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </form>
                            </div>
                        </div>
                    </div>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- third party js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->

    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>

    <script>
        if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
