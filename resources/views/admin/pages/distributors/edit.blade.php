@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.distributors.index') }}">@lang('admin/menu.distributor_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.distributor_management_create')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.distributor_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.distributor_management_create')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.distributors.update', $distributor->id) }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="type">@lang('admin/label.distributor_type')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    id="type"
                                    name="type"
                                    required>
                                <option value="">@lang('admin/placeholder.distributor_type')</option>
                                <option value="office" {{ $distributor->type === 'office' ? 'selected' : '' }}>Office</option>
                                <option value="factory" {{ $distributor->type === 'factory' ? 'selected' : '' }}>Factory</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="company-name">@lang('admin/label.distributor_company')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="company-name"
                                   name="company_name"
                                   value="{{ old('company_name', $distributor->company_name) }}"
                                   placeholder="@lang('admin/placeholder.distributor_company')"
                                   required/>
                        </div>

                        <div class="form-group">
                            <label for="address">@lang('admin/label.distributor_address')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control"
                                      id="address"
                                      name="address"
                                      cols="30"
                                      rows="10"
                                      required>{{ old('address', $distributor->address) }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="phone">@lang('admin/label.distributor_phone')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="phone"
                                   name="phone"
                                   value="{{ old('phone', $distributor->phone) }}"
                                   placeholder="@lang('admin/placeholder.distributor_phone')"
                                   required/>
                        </div>

                        <div class="form-group">
                            <label for="fax">@lang('admin/label.distributor_fax')</label>
                            <input type="text"
                                   class="form-control"
                                   id="fax"
                                   name="fax"
                                   value="{{ old('fax', $distributor->fax) }}"
                                   placeholder="@lang('admin/placeholder.distributor_fax')"/>
                        </div>

                        <div class="form-group">
                            <label for="facebook">@lang('admin/label.distributor_facebook')</label>
                            <input type="text"
                                   class="form-control"
                                   id="facebook"
                                   name="facebook"
                                   value="{{ old('facebook', $distributor->facebook) }}"
                                   placeholder="@lang('admin/placeholder.distributor_facebook')"/>
                        </div>

                        <div class="form-group">
                            <label for="email">@lang('admin/label.distributor_email')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="email"
                                   name="email"
                                   value="{{ old('email', $distributor->email) }}"
                                   placeholder="@lang('admin/placeholder.distributor_email')"
                                   required/>
                        </div>

                        <div class="form-group">
                            <label for="embed-url">@lang('admin/label.distributor_embed_url')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="embed-url"
                                   name="google_map_embed_url"
                                   value="{{ old('google_map_embed_url', $distributor->google_map_embed_url) }}"
                                   placeholder="@lang('admin/placeholder.distributor_embed_url')"
                                   required/>
                        </div>

                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.distributors.index') }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
