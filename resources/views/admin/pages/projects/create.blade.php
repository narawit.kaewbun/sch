@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="mt-3">
        <h3>@lang('admin/label.project_pattern')</h3>
        <div class="d-flex justify-content-center">
            <div class="p-2">
                <h5>แบบ A</h5>
                <img src="{{ asset('images/Artboard1.jpg') }}" alt="" class="img-fluid" width="300"><br><br>
                <p>SIZE A</p>
                <ul>
                    <li><b>A01</b> 800*800px</li>
                    <li><b>A02</b> 800*400px</li>
                    <li><b>A03</b> 400*400px</li>
                    <li><b>A04</b> 400*400px</li>
                    <li><b>A05</b> 400*800px</li>
                    <li><b>A06</b> 400*400px</li>
                    <li><b>A07</b> 400*400px</li>
                </ul>
            </div>
            <div class="p-2">
                <h5>แบบ B</h5>
                <img src="{{ asset('images/Artboard2.jpg') }}" alt="" class="img-fluid" width="300"><br><br>
                <p>SIZE B</p>
                <ul>
                    <li><b>B01</b> 800*400px</li>
                    <li><b>B02</b> 400*400px</li>
                    <li><b>B03</b> 400*400px</li>
                    <li><b>B04</b> 400*800p</li>
                    <li><b>B05</b> 800*800px</li>
                    <li><b>B06</b> 400*800px</li>
                </ul>
            </div>
            <div class="p-2">
                <h5>แบบ C</h5>
                <img src="{{ asset('images/Artboard3.jpg') }}" alt="" class="img-fluid" width="300"><br><br>
                <p>SIZE C</p>
                <ul>
                    <li><b>C01</b> 800*800px</li>
                    <li><b>C02</b> 400*400px</li>
                    <li><b>C03</b> 400*400px</li>
                    <li><b>C04</b> 400*800p</li>
                    <li><b>C05</b> 800*400px</li>
                    <li><b>C06</b> 400*400px</li>
                    <li><b>C07</b> 400*400px</li>
                </ul>
            </div>
            <div class="p-2">
                <h5>แบบ D</h5>
                <img src="{{ asset('images/Artboard4.jpg') }}" alt="" class="img-fluid" width="300"><br><br>
                <p>SIZE D</p>
                <ul>
                    <li><b>D01</b> 400*800px</li>
                    <li><b>D02</b> 800*800px</li>
                    <li><b>D03</b> 400*400px</li>
                    <li><b>D04</b> 400*400px</li>
                    <li><b>D05</b> 800*800px</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.project_management.index', $type) }}">@lang("admin/menu.project_management_${type}")</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.project_management_create')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang("admin/menu.project_management_{$type}")</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3 header-title">@lang('admin/menu.project_example')</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png"
                                 alt=""
                                 class="img-rounded w-100 image-preview">
                        </div>
                        <div class="col-lg-6">
                            <h2 class="mt-0 text-uppercase" id="name-preview"></h2>
                            <h5 class="text-uppercase sub-header" id="location-preview"></h5>
                            <div id="extra-preview-container" class="mt-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.project_management_create')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.project_management.store') }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        <input type="hidden" name="type" value="{{ $type }}">
                        <div class="form-group">
                            <label for="name">@lang('admin/label.project_name')&nbsp;<span class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="name"
                                   name="name"
                                   value="{{ old('name') }}"
                                   placeholder="@lang('admin/placeholder.project_name')"
                                   required
                                   oninput="onCreateProjectInputChange(this)"/>
                        </div>
                        <div class="form-group">
                            <label for="location">@lang('admin/label.project_location')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="location"
                                   name="location"
                                   value="{{ old('location') }}"
                                   placeholder="@lang('admin/placeholder.project_location')"
                                   required
                                   oninput="onCreateProjectInputChange(this)"/>
                        </div>
                        <div class="form-group">
                            <label for="pattern">@lang('admin/label.project_pattern')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    id="pattern"
                                    name="pattern"
                                    onchange="getPositionsViaPattern(this, '{{ $type }}')"
                                    required>
                                <option value="">@lang('admin/placeholder.project_pattern')</option>
                                @foreach (array_keys(config("patterns.{$type}")) as $item)
                                    <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="position">@lang('admin/label.project_position_in_pattern')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    id="position"
                                    name="position"
                                    required>
                                <option value="">@lang('admin/placeholder.project_position')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('admin/label.project_cover_picture')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="file"
                                   class="form-control-file"
                                   name="cover_image"
                                   value="{{ old('cover_image') }}"
                                   required
                                   oninput="onCreateProjectInputChange(this)">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('admin/label.project_content_picture')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="file"
                                   class="form-control-file"
                                   name="content_image"
                                   value="{{ old('content_image') }}"
                                   required
                                   oninput="onCreateProjectInputChange(this)">
                        </div>
                        <div id="extra-field-container"></div>
                        <button type="button" class="btn btn-dark" onclick="addField()">@lang('admin/button.add_field')</button>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.project_management.index', $type) }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
