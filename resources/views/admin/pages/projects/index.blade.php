@extends('admin.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang("admin/menu.project_management_{$type}")</li>
                    </ol>
                </div>
                <div class="d-flex align-items-center">
                    <h4 class="page-title">@lang("admin/menu.project_management_{$type}")</h4>
                    <a href="{{ route('admin.project_management.create', $type) }}" class="btn btn-primary ml-4">@lang('admin/button.add_project')</a>
                    <a href="{{ route('admin.project_management.exist_create', $type) }}" class="btn btn-secondary ml-4">@lang('admin/button.add_existing_project')</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="project-datatable" class="table dt-responsive nowrap table-borderless table-striped" style="width:100%">
                        <thead>
                        <tr>
                            <th>@lang('admin/tableHeader.project_name')</th>
                            <th>@lang('admin/tableHeader.project_pattern')</th>
                            <th>@lang('admin/tableHeader.project_position')</th>
                            <th>@lang('admin/tableHeader.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->name }}</td>
                                <td>{{ $type === 'all' ? $project->pattern : $project["{$type}_pattern"] }}</td>
                                <td>{{ $type === 'all' ? $project->position : $project["{$type}_position"] }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('admin.project_management.edit', [$type, $project->id]) }}">@lang('admin/button.edit')</a>
                                    <a class="btn btn-danger" href="javascript:void(0)" onclick="deleteProject({{ $project->id }})">@lang('admin/button.delete')</a>
                                    <form class="d-none" id="delete-project-form-{{ $project->id }}" action="{{ route('admin.project_management.destroy', [$project->id]) }}" method="POST">
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- third party js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->

    <script>
        // Initial Datatable
        $('#project-datatable').DataTable({
            "order": [[1, 'asc'], [2, 'asc']],
            "language": {
                "paginate": {
                    "previous": "<i class='mdi mdi-chevron-left'>",
                    "next": "<i class='mdi mdi-chevron-right'>"
                }
            },
            "drawCallback": function drawCallback() {
                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            },
            "columnDefs": [
                {"className": "text-center", "targets": "_all"},
                {"orderable": false, "targets": [3]}
            ],
        });

        // Delete project function
        function deleteProject(id) {
            Swal.fire({
                title: '@lang('admin/sweetAlert.warning_title')',
                text: '@lang('admin/sweetAlert.delete_project_warning')',
                type: 'warning',
                confirmButtonText: '@lang('admin/button.submit')',
                cancelButtonText: '@lang('admin/button.cancel')',
                confirmButtonClass: 'btn btn-confirm mt-2',
                cancelButtonClass: 'btn btn-danger ml-2 mt-2',
                buttonsStyling: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                if (result.value) {
                    $(`#delete-project-form-${id}`).submit();
                }
            })
        }

        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/success.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonText: '@lang('admin/button.submit')',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                }
            );
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/error.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
