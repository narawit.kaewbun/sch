@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.project_management.index', 'all') }}">@lang('admin/menu.project_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.project_management_edit')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.project_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3 header-title">@lang('admin/menu.project_example')</h4>
                    <div class="row">
                        <div class="col-lg-5">
                            <img id="image-preview"
                                 src="{{ asset($project->content_image) }}"
                                 alt=""
                                 class="img-rounded w-100">
                        </div>
                        <div class="col-lg-7">
                            <h2 class="mt-0 text-uppercase" id="name-preview">{{ $project->name }}</h2>
                            <h5 class="text-uppercase sub-header" id="location-preview">{{ $project->location }}</h5>
                            <div id="extra-preview-container" class="mt-4">
                                @if($project->data)
                                    @foreach($project->data as $key => $value)
                                        <div class="row mb-2" id="extra-preview-cover-{{ isset($value[2]) ? $value[2] : $value[0] }}">
                                            <div class="col-md-6 font-weight-bold text-capitalize" id="{{ isset($value[2]) ? $value[2] : $value[0] }}">{{ $value[0] }}</div>
                                            <div class="col-md-6 text-capitalize" id="{{ isset($value[2]) ? $value[2] : $value[0] }}-preview">{{ $value[1] }}</div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.project_management_edit')</h4>
                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.project_management.update', [$project->id]) }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        @method('put')
                        <input type="hidden" name="type" value="all">
                        <div class="form-group">
                            <label for="name">@lang('admin/label.project_name')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="name"
                                   name="name"
                                   value="{{ old('name', $project->name) }}"
                                   placeholder="@lang('admin/placeholder.project_name')"
                                   oninput="onCreateProjectInputChange(this)"/>
                        </div>
                        <div class="form-group">
                            <label for="location">@lang('admin/label.project_location')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="location"
                                   name="location"
                                   value="{{ old('location', $project->location) }}"
                                   placeholder="@lang('admin/placeholder.project_location')"
                                   oninput="onCreateProjectInputChange(this)"/>
                        </div>
                        <div class="form-group">
                            <label for="pattern">@lang('admin/label.project_pattern')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    id="pattern"
                                    name="pattern"
                                    onchange="getPositionsViaPattern(this, '{{ $type }}', true)"
                            >
                                <option value="">@lang('admin/placeholder.project_pattern')</option>
                                @foreach (array_keys(config("patterns.{$type}")) as $item)
                                    <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="position">@lang('admin/label.project_position_in_pattern')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    id="position"
                                    name="position">
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="d-flex flex-column align-items-start">
                                <label for="exampleInputEmail1">@lang('admin/label.project_cover_picture')</label>
                                <img src="{{ asset($project->cover_image) }}" alt="" class="img-fluid">
                                <input type="file"
                                       class="form-control-file"
                                       name="cover_image"
                                       value="{{ old('cover_image') }}"
                                       oninput="onCreateProjectInputChange(this)"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex flex-column align-items-start">
                                <label for="exampleInputEmail1">@lang('admin/label.project_content_picture')</label>
                                <img src="{{ asset($project->content_image) }}" alt="" class="img-fluid">
                                <input type="file"
                                       class="form-control-file"
                                       name="content_image"
                                       value="{{ old('content_image') }}"
                                       oninput="onCreateProjectInputChange(this)"/>
                            </div>
                        </div>
                        @if($project->data)
                            @foreach($project->data as $key => $value)
                                <div class="form-group d-flex align-items-end" id="extra-field-cover-{{ isset($value[2]) ? $value[2] : $value[0] }}">
                                    <div class="d-flex flex-column w-100">
                                        <label
                                            for="{{ isset($value[2]) ? $value[2] : $value[0] }}">{{ $value[0] }}</label>
                                        <input type="text"
                                               class="form-control"
                                               id="{{ isset($value[2]) ? $value[2] : $value[0] }}"
                                               name="{{ isset($value[2]) ? $value[2] : $value[0] }}"
                                               value="{{ old(isset($value[2]) ? $value[2] : $value[0], $value[1]) }}"
                                               oninput="onCreateProjectInputChange(this)"/>
                                    </div>
                                    <button type="button"
                                            class="btn btn-danger ml-md-2"
                                            data-extra-cover-id="{{isset($value[2]) ? $value[2] : $value[0]}}"
                                            onclick="deleteField(this)">@lang('admin/button.delete')
                                    </button>
                                </div>
                            @endforeach
                        @endif
                        <div id="extra-field-container"></div>
                        <button type="button" class="btn btn-dark" onclick="addField()">@lang('admin/button.add_field')</button>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>

                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script-bottom')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.project_management.index', 'all') }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }


        $('#pattern > *').each(function(key, ele) {
            if ('{{ $type }}' === 'all') {
                if ('{{ $project->pattern }}' === $(ele).val()) {
                    $(ele).attr('selected', 'selected');
                }
            } else {
                if ('{{ $project["{$type}_pattern"] }}' === $(ele).val()) {
                    $(ele).attr('selected', 'selected');
                }
            }
        });

        if ('{{ $type }}' === 'all') {
            window.getPositionsViaPattern($('#pattern'), '{{ $type }}', '{{ $project->position }}', true);
        } else {
            window.getPositionsViaPattern($('#pattern'), '{{ $type }}', '{{ $project["{$type}_position"]}}', true);
        }
    </script>
@endsection
