@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.project_management.index', $type) }}">@lang("admin/menu.project_management_{$type}")</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.project_management_create')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang("admin/menu.project_management_{$type}")</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.project_management_create')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.project_management.exist_store') }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        <input type="hidden" name="type" value="{{ $type }}">
                        <div class="form-group">
                            <label for="project">@lang('admin/label.project')</label>
                            <select class="form-control"
                                    id="project"
                                    name="project"
                                    required>
                                <option value="">@lang('admin/placeholder.project')</option>
                                @foreach ($projects as $project)
                                    <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pattern">@lang('admin/label.project_pattern')</label>
                            <select class="form-control"
                                    id="pattern"
                                    name="pattern"
                                    onchange="getPositionsViaPattern(this, '{{ $type }}')"
                                    required>
                                <option value="">@lang('admin/placeholder.project_pattern')</option>
                                @foreach (array_keys(config("patterns.{$type}")) as $item)
                                    <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="position">@lang('admin/label.project_position_in_pattern')</label>
                            <select class="form-control"
                                    id="position"
                                    name="position"
                                    required>
                                <option value="">@lang('admin/placeholder.project_position')</option>
                            </select>
                        </div>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: 'สำเร็จ',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.project_management.index', $type) }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: 'ผิดพลาด',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
