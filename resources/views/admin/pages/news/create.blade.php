@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.news.index') }}">@lang('admin/menu.news_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.news_management_create')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.news_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.news_management_create')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.news.store') }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('admin/label.news_cover')&nbsp;<span class="text-danger">*</span></label>
                            <input type="file"
                                   class="form-control-file"
                                   name="cover"
                                   value="{{ old('cover') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="en_title">@lang('admin/label.news_en_title')&nbsp;<span class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="en_title"
                                   name="en_title"
                                   placeholder="@lang('admin/placeholder.news_management_title')"
                                   required/>
                        </div>
                        <div class="form-group">
                            <label for="th_title">@lang('admin/label.news_th_title')&nbsp;<span class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="th_title"
                                   name="th_title"
                                   placeholder="@lang('admin/placeholder.news_management_title')"
                                   required/>
                        </div>
                        <div class="form-group">
                            <label for="image-0">@lang('admin/label.news_picture')&nbsp;<span class="text-danger">*</span></label>
                            <input type="file"
                                   class="form-control-file"
                                   name="image-0"
                                   oninput="onCreateContentInputChange(this)"
                                   required>
                        </div>
                        <div id="extra-images-container"></div>
                        <button type="button" class="btn btn-dark mb-2" onclick="addNewsImage()">@lang('admin/button.add_news_picture')</button>
                        <div class="form-group">
                            <label for="en_summernote-editor">@lang('admin/label.news_en_content')&nbsp;<span class="text-danger">*</span></label>
                            <textarea id="en_summernote-editor" name="en_content" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="th_summernote-editor">@lang('admin/label.news_th_content')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <textarea id="th_summernote-editor" name="th_content" required></textarea>
                        </div>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/katex/katex.min.js')}}"></script>
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js')}}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/pages/form-summernote.init.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>

    <script>
        let extraImageCount = 0;

        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.news.index') }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }

        $('#en_summernote-editor').summernote({
            height: 400,
            // set editor height
            minHeight: null,
            // set minimum height of editor
            maxHeight: null,
            // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('#th_summernote-editor').summernote({
            height: 400,
            // set editor height
            minHeight: null,
            // set minimum height of editor
            maxHeight: null,
            // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        function addNewsImage() {
            extraImageCount += 1;

            let html = `
                <div class="form-group d-flex flex-column flex-md-row" id=extra-image-cover-${extraImageCount}>
                    <div class="flex-grow-1">
                            <div class="form-group">
                                <label for="image-${extraImageCount}">@lang('admin/label.news_picture')&nbsp;<span
                                        class="text-danger">*</span></label>
                                <input type="file"
                                       class="form-control-file"
                                       name="image-${extraImageCount}"
                                       oninput="onCreateContentInputChange(this)"
                                       required>
                            </div>
                      </div>
                      <button type="button" class="btn btn-danger ml-md-2" data-extra-image-id="${extraImageCount}" onclick="deleteImage(this)">@lang('admin/button.delete')</button>
                </div>
            `;

            $('#extra-images-container').append(html);
        }


        function deleteImage(e) {
            const coverId = $(e).data('extra-image-id');

            $(`#extra-image-cover-${coverId}`).remove();
        }
    </script>
@endsection
