@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.contents.first_page.slide_index') }}">@lang('admin/menu.slide_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.slide_management_create')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.slide_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.slide_management_create')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.contents.first_page.slide_store') }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        <div class="form-group">
                            <label for="sliders-0">@lang('admin/label.slide_position')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    name="slide-0"
                                    id="sliders-0"
                                    onChange="onSelectChange(this)"
                                    required>
                                <option value="">@lang('admin/placeholder.slide_position')</option>
                                @for($i = 1; $i <= $slideCounts + 1; $i++)
                                    <option value="{{ $i }}" {{ $i == ($slideCounts + 1) ? 'selected' : ''}}>{{ $i }}</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="image-0">@lang('admin/label.content_management_image')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="file"
                                   class="form-control-file"
                                   name="image-0"
                                   oninput="onCreateContentInputChange(this)"
                                   required>
                        </div>
                        <div id="extra-slides-container"></div>
                        <button type="button" class="btn btn-dark" onclick="addSlide()">@lang('admin/button.add_slide')</button>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        let extraSlideCount = 0;
        let extraOptions;
        let extraSlidersCount = $('[id^=sliders-]').length - 1;
        const baseOptionsInitial = $('#sliders-0').children().length - 1;

        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.contents.first_page.slide_index') }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }


        function addSlide(e) {
            extraSlideCount += 1;

            let html = `
                <div class="form-group d-flex flex-column flex-md-row" id=extra-slide-cover-${extraSlideCount}>
                    <div class="flex-grow-1">
                        <div class="form-group">
                                <label for="sliders-${extraSlideCount}">@lang('admin/label.slide_position')</label>
                                <select class="form-control"
                                        name="slide-${extraSlideCount}"
                                        id="sliders-${extraSlideCount}"
                                        onChange="onSelectChange(this)"
                                        required>
                                    <option value="">@lang('admin/placeholder.slide_position')</option>
                                    <option value="1">1</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image-${extraSlideCount}">@lang('admin/label.content_management_image')&nbsp;<span
                                        class="text-danger">*</span></label>
                                <input type="file"
                                       class="form-control-file"
                                       name="image-${extraSlideCount}"
                                       oninput="onCreateContentInputChange(this)"
                                       required>
                            </div>
                      </div>
                      <button type="button" class="btn btn-danger ml-md-2" data-extra-slide-id="${extraSlideCount}" onclick="deleteSlide(this)">@lang('admin/button.delete')</button>
                </div>
            `;

            $('#extra-slides-container').append(html);

            addExtraSlideOptions();
        }

        function onSelectChange(e) {
            // This will regenerate all selected options
            $('[id^=sliders-]').each((key, ele) => {
                if (e.id !== ele.id) {
                    if (ele.value === e.value) {
                        $(`#${ele.id}`).val('');
                        $(`#${ele.id} option`).first().prop('selected', true);
                    }
                }
            });
        }

        function deleteSlide(e) {
            const coverId = $(e).data('extra-slide-id');

            $(`#extra-slide-cover-${coverId}`).remove();

            deleteExtraSlideOptions();
        }

        function addExtraSlideOptions() {
            // -1 for base sliders
            let extraSlidersCount = $('[id^=sliders-]').length - 1;

            for (let i = 0; i <= extraSlideCount; i++) {
                extraOptions = '<option value="">@lang('admin/placeholder.slide_position')</option>';
                for (let j = 1; j <= (extraSlidersCount + baseOptionsInitial); j++) {

                    if ($(`#sliders-${i}`).val() == j) {
                        extraOptions += `
                        <option value="${j}" selected>${j}</option>
                    `;
                    } else {
                        extraOptions += `
                        <option value="${j}">${j}</option>
                    `;
                    }

                }

                $(`#sliders-${i}`).html(extraOptions);

            }
        }

        function deleteExtraSlideOptions() {
            // -1 for base sliders
            let extraSlidersCount = $('[id^=sliders-]').length - 1;

            for (let i = 0; i <= extraSlideCount; i++) {
                extraOptions = '<option value="">@lang('admin/placeholder.slide_position')</option>';
                for (let j = 1; j <= (extraSlidersCount + baseOptionsInitial); j++) {

                    if ($(`#sliders-${i}`).val() == j) {
                        extraOptions += `
                        <option value="${j}" selected>${j}</option>
                    `;
                    } else {
                        extraOptions += `
                        <option value="${j}">${j}</option>
                    `;
                    }

                }

                $(`#sliders-${i}`).html(extraOptions);
            }
        }

    </script>
@endsection
