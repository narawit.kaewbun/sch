@extends('admin.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin/menu.content_management_first_page')</li>
                    </ol>
                </div>
                <div class="d-flex align-items-center">
                    <h4 class="page-title">@lang('admin/menu.content_management_first_page')</h4><a href="{{ route('admin.contents.first_page.create') }}" class="btn btn-primary ml-4">สร้างเนื้อหา</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="project-datatable" class="table dt-responsive nowrap table-borderless table-striped" style="width:100%">
                        <thead>
                        <tr>
                            <th>@lang('admin/tableHeader.content_en_title')</th>
                            <th>@lang('admin/tableHeader.content_status')</th>
                            <th>@lang('admin/tableHeader.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contents as $content)
                            <tr>
                                <td>{{ $content->en_title }}</td>
                                <td><p class="text-{{ $content->is_hidden == false ? 'success' : 'danger' }}">{{ ucwords($content->is_hidden == false ? 'enabled' : 'disabled') }}</p></td>
                                <td>
                                    <a class="btn btn-{{ $content->is_hidden ? 'success' : 'secondary' }}"
                                       href="javascript:void(0)"
                                       onclick="handleEnablingOrDisabling({{ $content->is_hidden }}, {{ $content->id }})">{{ $content->is_hidden ? trans('admin/button.enable') : trans('admin/button.disable') }}</a>
                                    <a class="btn btn-primary" href="{{ route('admin.contents.first_page.edit', [$content->id]) }}">@lang('admin/button.edit')</a>
                                    <form id="update-visibility-form-{{ $content->id }}" class="d-none" action="{{ route('admin.contents.visible', [$content->id]) }}" method="POST">
                                        @method('put')
                                        @csrf
                                    </form>
                                    <a class="btn btn-danger" href="javascript:void(0)" onclick="deleteDownloadFile({{ $content->id }})">@lang('admin/button.delete')</a>
                                    <form id="delete-content-form-{{ $content->id }}" class="d-none" action="{{ route('admin.contents.destroy', [$content->id]) }}" method="POST">
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- third party js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->

    <script>
        // Initial Datatable
        $('#project-datatable').DataTable({
            "language": {
                "paginate": {
                    "previous": "<i class='mdi mdi-chevron-left'>",
                    "next": "<i class='mdi mdi-chevron-right'>"
                }
            },
            "drawCallback": function drawCallback() {
                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            },
            "columnDefs": [
                {"className": "text-center", "targets": "_all"},
                {"orderable": false, "targets": "_all"},
                {"orderable": true, "targets": 0}
            ],
        });

        function deleteDownloadFile(id) {
            Swal.fire({
                title: 'คำเตือน',
                text: 'คุณยืนยันที่จะลบเนื้อหานี้',
                type: 'warning',
                confirmButtonText: '@lang('admin/button.submit')',
                cancelButtonText: '@lang('admin/button.cancel')',
                confirmButtonClass: 'btn btn-confirm mt-2',
                cancelButtonClass: 'btn btn-danger ml-2 mt-2',
                buttonsStyling: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                if (result.value) {
                    $(`#delete-content-form-${id}`).submit();
                }
            })
        }

        function handleEnablingOrDisabling(isHidden, id) {
            Swal.fire({
                title: 'คำเตือน',
                text: isHidden ? 'คุนยืนยันที่จะเปิดการใช้งาน' : 'คุณยืนยันที่จะปิดการใช้งาน',
                type: 'warning',
                confirmButtonText: '@lang('admin/button.submit')',
                cancelButtonText: '@lang('admin/button.cancel')',
                confirmButtonClass: 'btn btn-confirm mt-2',
                cancelButtonClass: 'btn btn-danger ml-2 mt-2',
                buttonsStyling: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                if (result.value) {
                    $(`#update-visibility-form-${id}`).submit();
                }
            })
        }

        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: 'สำเร็จ',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonText: '@lang('admin/button.submit')',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                }
            );
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: 'ผิดพลาด',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
