@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.contents.index', [$type]) }}">@lang('admin/menu.content_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.content_management_edit')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.content_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3 header-title">@lang('admin/menu.project_example')</h4>
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-uppercase type-preview">
                                @if($content->type === 'extended-service')
                                    @php($strings = explode('-', $content->type))
                                    {{ ucwords($strings[0] . ' ' . $strings[1]) }}
                                @else
                                    {{ ucfirst($type) }}
                                @endif
                            </h2>
                        </div>
                        <div class="col-lg-5">
                            <img src="{{ asset($content->image) }}"
                                 alt=""
                                 class="img-rounded w-100 image-preview">
                        </div>
                        <div class="col-lg-7">
                            <h3 class="mt-0 text-uppercase title-preview">{{ $content->en_title }}</h3>
                            <p id="en_content-preview">{{ $content->en_content }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3 header-title">@lang('admin/menu.project_example')</h4>
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-uppercase type-preview">
                                @if($content->type === 'extended-service')
                                    @php($strings = explode('-', $content->type))
                                    {{ ucwords($strings[0] . ' ' . $strings[1]) }}
                                @else
                                    {{ ucfirst($type) }}
                                @endif
                            </h2>
                        </div>
                        <div class="col-lg-5">
                            <img  src="{{ asset($content->image) }}"
                                  alt=""
                                  class="img-rounded w-100 image-preview">
                        </div>
                        <div class="col-lg-7">
                            <h3 class="mt-0 text-uppercase title-preview">{{ $content->en_title }}</h3>
                            <span id="th_title-preview">{{ $content->th_title }}</span>
                            <p id="th_content-preview">{{ $content->th_content }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.content_management_edit')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.contents.update', [$content->id]) }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="type">@lang('admin/label.content_management_type')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <select class="form-control"
                                    id="type"
                                    name="type"
                                    onchange="onEditContentInputChange(this)"
                                    required>
                                <option value="">@lang('admin/placeholder.content_management_type')</option>
                                @foreach($types as $key => $value)
                                    <option value="{{ $value }}" {{ $value === old('type', $content->type) ? 'selected' : ''}} >
                                        @if($value === 'extended-service')
                                            @php($strings = explode('-', $value))
                                            {{ ucwords($strings[0] . ' ' . $strings[1]) }}
                                        @else
                                            {{ ucfirst($value) }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">@lang('admin/label.content_management_en_title')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="title"
                                   name="en_title"
                                   oninput="onEditContentInputChange(this)"
                                   value="{{ old('en_title', $content->en_title) }}"
                                   placeholder="@lang('admin/placeholder.content_management_title')"
                                   required/>
                        </div>

                        <div class="form-group">
                            <label for="th_title">@lang('admin/label.content_management_th_title')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="th_title"
                                   name="th_title"
                                   oninput="onEditContentInputChange(this)"
                                   value="{{ old('th_title', $content->th_title) }}"
                                   placeholder="@lang('admin/placeholder.content_management_title')"
                                   required/>
                        </div>

                        <div class="form-group">
                            <label for="en_content">@lang('admin/label.content_management_en_content')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control"
                                      id="en_content"
                                      name="en_content"
                                      cols="30"
                                      rows="10"
                                      oninput="onEditContentInputChange(this)"
                                      required>{{ old('en_content', $content->en_content) }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="th_content">@lang('admin/label.content_management_th_content')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <textarea class="form-control"
                                      id="th_content"
                                      name="th_content"
                                      cols="30"
                                      rows="10"
                                      oninput="onEditContentInputChange(this)"
                                      required>{{ old('th_content', $content->th_content) }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="image">@lang('admin/label.content_management_image')</label>
                            <input type="file"
                                   class="form-control-file"
                                   name="image"
                                   oninput="onEditContentInputChange(this)"
                                   value="{{ old('cover') }}">
                        </div>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.contents.index', [$type]) }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }

        function onEditContentInputChange(e) {
            if (e.name === 'image') {
                setPreviewImage(e, e.name);
            } else {
                if (e.id === 'title' || e.id === 'type') {
                    $(`.${e.id}-preview`).each(function(index, item) {
                        $(item).text(e.value);
                    })
                }
                $(`#${e.id}-preview`).text(e.value);
            }
        }
    </script>
@endsection
