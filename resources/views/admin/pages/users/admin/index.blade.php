@extends('admin.layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin/menu.admin_management')</li>
                    </ol>
                </div>
                <div class="d-flex align-items-center">
                    <h4 class="page-title">@lang('admin/menu.admin_management')</h4><a href="{{ route('admin.admin_management.create') }}" class="btn btn-primary ml-4">เพิ่มผู้ดูแลระบบ</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="admin-datatable" class="table dt-responsive nowrap table-borderless table-striped" style="width:100%">
                        <thead>
                        <tr>
                            <th>@lang('admin/tableHeader.username')</th>
                            <th>@lang('admin/tableHeader.email')</th>
                            <th>@lang('admin/tableHeader.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <button type="button" class="btn btn-primary waves-effect waves-light"
                                            data-toggle="modal"
                                            data-target="#con-close-modal"
                                            data-user-id="{{ $user->id }}"
                                            onclick="setUserIdToChangePasswordForm(this)">@lang('admin/button.change_password')</button>
                                    <a class="btn btn-danger" href="javascript:void(0)" onclick="deleteAdminUser()">@lang('admin/button.delete')</a>
                                    <form class="d-none" id="delete-user-form" action="{{ route('admin.admin_management.destroy', [$user->id]) }}" method="POST">
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form id="changePasswordForm" method="POST" class="parsley-examples" autocomplete="off">
                                    @csrf
                                    @method('put')
                                    <div class="modal-header">
                                        <h4 class="modal-title">@lang('admin/label.change_password')</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                        </button>
                                    </div>
                                    <div class="modal-body p-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="new_password"
                                                           class="control-label">@lang('admin/label.new_password')</label>
                                                    <input type="password"
                                                           class="form-control"
                                                           id="new_password"
                                                           name="new_password"
                                                           placeholder="@lang('admin/placeholder.new_password')"
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="new_password_confirmation"
                                                           class="control-label">@lang('admin/label.new_password_confirmation')</label>
                                                    <input type="password"
                                                           class="form-control"
                                                           id="new_password_confirmation"
                                                           name="new_password_confirmation"
                                                           placeholder="@lang('admin/placeholder.new_password_confirmation')"
                                                           data-parsley-equalto="#new_password"
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary waves-effect"
                                                data-dismiss="modal">@lang('admin/button.cancel')</button>
                                        <button type="submit"
                                                class="btn btn-success waves-effect waves-light">@lang('admin/button.submit')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- /.modal -->

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@section('script')
    <!-- third party js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->

    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>

    <script>
        // Initialize datatable
        $('#admin-datatable').DataTable({
            "language": {
                "paginate": {
                    "previous": "<i class='mdi mdi-chevron-left'>",
                    "next": "<i class='mdi mdi-chevron-right'>"
                }
            },
            "drawCallback": function drawCallback() {
                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            },
            "columnDefs": [
                {"className": "text-center", "targets": "_all"},
                {"orderable": false, "targets": [2]}
            ],
        });

        function setUserIdToChangePasswordForm(e) {
            let route = '{{ route('admin.admin_management.change_password', ['user' => '_user']) }}';
            route = route.replace('_user', $(e).data('user-id'));
            $('#changePasswordForm').attr('action', route);
        }

        // Delete project function
        function deleteAdminUser() {
            Swal.fire({
                title: '@lang('admin/sweetAlert.warning_title')',
                text: '@lang('admin/sweetAlert.delete_admin_warning')',
                type: 'warning',
                confirmButtonText: '@lang('admin/button.submit')',
                cancelButtonText: '@lang('admin/button.cancel')',
                confirmButtonClass: 'btn btn-confirm mt-2',
                cancelButtonClass: 'btn btn-danger ml-2 mt-2',
                buttonsStyling: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                if (result.value) {
                    $('#delete-user-form').submit();
                }
            })
        }

        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            );
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }
    </script>
@endsection
