@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.admin_management.index') }}">@lang('admin/menu.admin_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.admin_management_create')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.admin_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.admin_management_create')</h4>

                    @if(session()->has('success'))
                        <div class="text-center text-success my-2">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.admin_management.store') }}"
                          method="POST"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        <div class="form-group">
                            <label for="email">@lang('admin/label.email')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="email"
                                   class="form-control"
                                   id="email"
                                   name="email"
                                   value="{{ old('email') }}"
                                   placeholder="@lang('admin/placeholder.email')"
                                   required/>
                        </div>
                        <div class="form-group">
                            <label for="username">@lang('admin/label.username')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="username"
                                   name="username"
                                   value="{{ old('username') }}"
                                   placeholder="@lang('admin/placeholder.username')"
                                   required/>
                        </div>
                        <div class="form-group">
                            <label for="password">@lang('admin/label.password')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="password"
                                   class="form-control"
                                   id="password"
                                   name="password"
                                   value="{{ old('password') }}"
                                   placeholder="@lang('admin/placeholder.password')"
                                   required/>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">@lang('admin/label.password_confirmation')&nbsp;<span
                                    class="text-danger">*</span></label>
                            <input type="password"
                                   class="form-control"
                                   id="password_confirmation"
                                   name="password_confirmation"
                                   value="{{ old('password_confirmation') }}"
                                   placeholder="@lang('admin/placeholder.password_confirmation')"
                                   required/>
                        </div>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>

                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
@endsection
