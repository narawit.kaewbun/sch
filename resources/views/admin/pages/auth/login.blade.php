@extends('admin.layouts.master-without-nav')

@section('body')
    <body class="authentication-bg authentication-bg-pattern">
    @endsection

    @section('content')

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card bg-pattern">

                            <div class="card-body p-4">

                                <div class="text-center w-75 m-auto">
                                    <a href="index">
                                        <span><img src="{{ url('images/logo_sms.gif') }}" alt="" height="50"></span>
                                    </a>
                                    <p class="text-muted mb-2 mt-3">Enter your username and password to access
                                        admin panel.</p>
                                </div>

                                @foreach($errors->all() as $key => $value)
                                    <div class="text-center text-danger my-2">
                                        {{ $value }}
                                    </div>
                                @endforeach

                                <form action="{{ route('admin.auth.login') }}" method="POST">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <label for="username">Username</label>
                                        <input class="form-control"
                                               type="text"
                                               id="username"
                                               name="username"
                                               placeholder="Enter your username">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <input class="form-control"
                                               type="password"
                                               id="password"
                                               name="password"
                                               placeholder="Enter your password">
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-signin"
                                                   checked>
                                            <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit"> Log In</button>
                                    </div>

                                </form>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p><a href="pages-recoverpw" class="text-white-50 ml-1">Forgot your password?</a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt">
            2015 - {{date('Y')}} &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a>
        </footer>
@endsection

@section('script')
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            );
        }
    </script>
@endsection
