@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.index') }}">@lang('admin/menu.dashboard')</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.downloads.index') }}">@lang('admin/menu.download_management')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin/menu.download_management_edit')</li>
                    </ol>
                </div>
                <h4 class="page-title">@lang('admin/menu.download_management')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row justify-content-center flex-row-reverse">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3 header-title">@lang('admin/menu.project_example')</h4>
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <img id="cover-preview"
                                 src="{{ $download->cover }}"
                                 alt=""
                                 class="img-rounded w-100">
                            <p id="title-preview" class="m-0">{{ $download->title }}</p>
                            <p id="sub_title-preview">{{ $download->sub_title }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">@lang('admin/menu.download_management_edit')</h4>

                    @foreach($errors->all() as $key => $value)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $value }}
                        </div>
                    @endforeach

                    <form action="{{ route('admin.downloads.update', $download->id) }}"
                          method="POST"
                          enctype="multipart/form-data"
                          autocomplete="off"
                          class="parsley-examples">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="title">@lang('admin/label.download_title')&nbsp;<span class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="title"
                                   name="title"
                                   oninput="onEditDownloadInputChange(this)"
                                   value="{{ old('title', $download->title) }}"
                                   placeholder="@lang('admin/placeholder.download_title')"
                                   required/>
                        </div>

                        <div class="form-group">
                            <label for="sub_title">@lang('admin/label.download_sub_title')</label>
                            <input type="text"
                                   class="form-control"
                                   id="sub_title"
                                   name="sub_title"
                                   oninput="onEditDownloadInputChange(this)"
                                   value="{{ old('sub_title', $download->sub_title) }}"
                                   placeholder="@lang('admin/placeholder.download_sub_title')"/>
                        </div>

                        <div class="form-group">
                            <div class="d-flex flex-column align-items-start">
                                <label for="exampleInputEmail1">@lang('admin/label.download_cover')</label>
                                <input type="file"
                                       class="form-control-file"
                                       name="cover"
                                       value="{{ old('cover', $download->cover) }}"
                                       oninput="onEditDownloadInputChange(this)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('admin/label.download_file')</label>
                            <input type="file"
                                   class="form-control-file"
                                   name="file"
                                   value="{{ old('file') }}">
                        </div>
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light">@lang('admin/button.submit')</button>
                    </form>


                    <!-- -->
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                    </div>
                    <!-- -->
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->
    </div>
    <!-- end of row -->
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.min.js"></script>
    <!-- Plugin js-->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script>
        if ('{{ session()->has('success') }}' === '1') {
            Swal.fire(
                {
                    title: '@lang('admin/sweetAlert.success_title')',
                    text: '{{ session()->get('success') }}',
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2',
                    confirmButtonText: '@lang('admin/button.submit')',
                }
            ).then(() => {
                window.location.href = '{{ route('admin.downloads.index') }}';
            })
        } else if ('{{ session()->has('error') }}' === '1') {
            Swal.fire({
                title: '@lang('admin/sweetAlert.error_title')',
                text: '{{ session()->get('error') }}',
                type: 'error',
                confirmButtonClass: 'btn btn-confirm mt-2',
                confirmButtonText: '@lang('admin/button.submit')',
            });
        }

        function onEditDownloadInputChange(e) {
            if (e.name === 'image') {
                setPreviewImage(e, e.name);
            } else {
                $(`#${e.id}-preview`).text(e.value);
            }
        }
    </script>
@endsection
