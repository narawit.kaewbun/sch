@extends('admin.layouts.master')

@section('css')
    <!-- Plugins css -->
    <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">

    </div>
    <!-- end page title -->
@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>

    <!-- Dashboar 1 init js-->
    <script src="{{ URL::asset('assets/js/pages/dashboard-1.init.js')}}"></script>
@endsection
