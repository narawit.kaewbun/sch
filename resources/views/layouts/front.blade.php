<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mdb.min.css') }}">
    @yield('styles')

</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light navbar-sch py-0 py-sm-5">
        <div class="container nav-pdd">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('images/logo_sms.gif') }}" alt="logo-gif" class="img-fluid">
            </a>
            <button class="navbar-toggler order-first" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            EXTRUSION
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                            <div class="d-flex flex-column flex-sm-row">
                                @foreach($menus['extrusion'] as $key => $value)
                                <a class="dropdown-item" href="{{ route('products_and_service.index', $value) }}">{{ $key }}</a>
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            FINISHING
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                            <div class="d-flex flex-column flex-sm-row">
                                @foreach($menus['finishing'] as $key => $value)
                                <a class="dropdown-item" href="{{ route('products_and_service.index', $value) }}">{{ $key }}</a>
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            EXTENDED SERVICE
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                            <div class="d-flex flex-column flex-sm-row">
                                @foreach($menus['extended-service'] as $key => $value)
                                <a class="dropdown-item" href="{{ route('products_and_service.index', $value) }}">{{ $key }}</a>
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ARCHITECTURAL PRODUCTS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown4">
                            <div class="d-flex flex-column flex-sm-row">
                                <a class="dropdown-item" href="{{ url('architectural_product/building_system') }}">BUILDING SYSTEMS</a>
                                <a class="dropdown-item" href="{{ url('architectural_product/hardware') }}">HARDWARE</a>
                                <a class="dropdown-item" href="{{ url('architectural_product/research') }}">RESEARCH & DEVELOPMENT</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('projects.index') }}">PROJECTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('downloads.index') }}">DOWNLOAD</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            CONTACT US
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown4">
                            <div class="d-flex flex-column">
                                <a class="dropdown-item" href="{{ route('news.index') }}">NEWS</a>
                                <a class="dropdown-item" href="{{ route('contact.index') }}">CONTACT US</a>
                                <a class="dropdown-item" href="{{ route('about') }}">ABOUT US</a>
                                <a class="dropdown-item" href="{{ route('authorized') }}">AUTHORIZED DISTRIBUTOR</a>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="link-lang my-2 my-lg-0">
                    <ul class="navbar-nav mr-auto d-block d-sm-none">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('news.index') }}">NEWS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('contact.index') }}">CONTACT US</a>
                        </li>
                    </ul>
                    <div class="d-flex">
                        <div class="p-2">
                            <a href="https://www.facebook.com/Schimmer-Metal-Standard-1288490467930890" target="_blank"><i class="fab fa-facebook fa-lg"></i></a>
                        </div>
                        <div class="p-2">
                            <a href="https://www.youtube.com/channel/UCjEkTXlpZLNyg6d7pcsUEzw" target="_blank"><i class="fab fa-youtube fa-lg"></i></a>
                        </div>
                        <div class="p-2">
                            <a href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">English</a>
                        </div>
                        <div class="p-2">
                            |
                        </div>
                        <div class="p-2">
                            <a href="{{ LaravelLocalization::getLocalizedURL('th', null, [], true) }}">ภาษาไทย</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    @yield('content')

    <footer class="footer py-3 py-sm-5">
        <div class="container">
            <div class="d-flex flex-column flex-sm-row justify-content-between">
                <div class="p-2">
                    <a href="{{ route('news.index') }}">NEWS</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="{{ route('contact.index') }}">CONTACT US</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="{{ route('about') }}">ABOUT US</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="{{ route('authorized') }}">AUTHORIZED DISTRIBUTOR</a>
                </div>
                <div class="p-2">
                    2018 SMS Schimmer. All Rights Reserved
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/mdb.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/wow.min.js') }}" type="text/javascript"></script>
    @yield('scripts')
    <script>
        $(document).ready(function() {
            $('.dropdown-toggle').removeClass('waves-effect waves-light');
            $('.dropdown-item').removeClass('waves-effect waves-light');
        });
    </script>
    <script id="r-widget-script" src="https://rwidget.readyplanet.com/widget/widget.min.js?business_id=5ad2750932d7329d9985c9408c117c74" type="text/javascript" charset="UTF-8"></script>
</body>

</html>
