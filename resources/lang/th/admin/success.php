<?php

return [
    'success_title' => 'สำเร็จ',
    'update_project' => 'อัพเดทโปรเจกต์สำเร็จ',
    'store_project' => 'สร้างโปรเจกต์สำเร็จ',
    'delete_project' => 'ลบโปรเจกต์สำเร็จ',
    'change_password' => 'เปลี่ยนรหัสผ่านสำเร็จ กรุณาล็อกอินใหม่อีกครั้ง',
    'change_password_user' => 'เปลี่ยนรหัสผ่านสำเร็จ',
    'delete_user' => 'ลบผู้ดูแลระบบสำเร็จ',
    'create_download_page' => 'สร้างไฟล์ดาวน์โหลดสำเร็จ',
    'delete_download' => 'ลบไฟล์สำเร็จ',
    'create_content' => 'สร้างเนื้อหาสำเร็จ',
    'update_content' => 'แก้ไขเนื้อหาสำเร็จ',
    'delete_content' => 'ลบเนื้อหาสำเร็จ',
    'update_visibility' => 'อัพเดทสำเร็จ',
    'youtube_update_success' => 'อัพเดทลิงก์ยูทูปสำเร็จ',
    'news_update' => 'อัพเดทข่าวสำเร็จ',
    'create_news_success' => 'สร้างข่าวสำเร็จ',
    'delete_news' => 'ลบข่าวสำเร็จ',
    'download_update' => 'อัพโหลดไฟล์ดาวน์โหลดสำเร็จ',
    'create_distributor' => 'สร้างตัวแทนจำหน่ายสำเร็จ',
    'update_distributor' => 'อัพเดทสำเร็จ',
    'delete_distributor' => 'ลบตัวแทนจำหน่ายสำเร็จ',
];
