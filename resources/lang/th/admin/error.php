<?php

return [
    'error_title' => 'ผิดพลาด',
    'upload_image' => 'เกิดข้อผิดพลาดในการอัพโหลดไฟล์รูปภาพ',
    'current_password' => 'รหัสผ่านปัจจุบันไม่ถูกต้อง'
];
