<?php

return [
    'id' => 'ไอดี',
    'type' => 'ประเภท',
    'action' => 'คำสั่ง',
    'edit' => 'แก้ไข',
    'delete' => 'ลบ',
    'status' => 'สถานะ',
    'disabled' => 'ปิดใช้งาน',
    'enabled' => 'เปิดใช้งาน',
    'position' => 'ตำแหน่ง',
    // Admin Management
    'username' => 'ชื่อผู้ใช้',
    'email' => 'อีเมลล์',
    'change_password' => 'เปลี่ยนรหัสผ่าน',
    // Project Management
    'project_name' => 'ชื่อโปรเจกต์',
    'project_pattern' => 'แพทเทิร์น',
    'project_position' => 'ตำแหน่งในแพทเทิร์น',
    'project_image' => 'รูปภาพ',
    'download_title' => 'ชื่อ',
    'download_sub_title' => 'ชื่อรอง',
    'content_en_title' => 'ชื่ออังกฤษ',
    'content_th_title' => 'ชื่อไทย',
    'content_type' => 'ประเภท',
    'content_status' => 'สถานะ',
    // News Management
    'news_title' => 'ชื่อข่าว',
    // First Page Content
    'slide_position' => 'ตำแหน่งสไลด์',
    // Distributors
    'distributor_company_name' => 'ชื่อบริษัท',
    'distributor_type' => 'ประเภท'
];
