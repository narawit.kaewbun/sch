<?php

return [
    'success_title' => 'สำเร็จ',
    'error_title' => 'ผิดพลาด',
    'warning_title' => 'คำเตือน',
    'delete_project_warning' => 'คุณยืนยันที่จะลบโปรเจกต์นี้',
    'delete_admin_warning' => 'คุณยืนยันที่จะลบแอดมินในระบบ',
    'delete_file_warning' => 'คุณยืนยันที่จะลบไฟล์นี้',
    'delete_image_success' => 'ลบรูปภาพสำเร็จแล้ว',
    'delete_content_warning' => 'คุณยืนยันที่จะลบเนื้อหานี้',
    'enable_confirming_warning' => 'คุนยืนยันที่จะเปิดการใช้งาน',
    'disable_confirming_warning' => 'คุณยืนยันที่จะปิดการใช้งาน',
    'delete_project_ref_warning' => 'คุณยืนยันที่จะลบ Project Reference นี้',
    'delete_slide_warning' => 'คุณยืนยันที่จะลบสไลด์นี้',
    'delete_distributor_warning' => 'คุณยืนยันที่จะลบตัวแทนจำหน่าย'
];
