<?php

return [
    'email' => 'กรุณากรอกอีเมลล์',
    'username' => 'กรุณากรอกชื่อผู้ใช้',
    'current_password' => 'กรุณากรอกรหัสผ่านปัจจุบัน',
    'new_password' => 'กรุณากรอกรหัสผ่านใหม่',
    'new_password_confirmation' => 'กรุณากรอกรหัสผ่านอีกครั้ง',
    'password' => 'กรุณากรอกรหัสผ่าน',
    'password_confirmation' => 'กรุณากรอกรหัสผ่านอีกครั้ง',
    'project' => 'กรุณาเลือกโปรเจกต์',
    'project_name' => 'กรุณากรอกชื่อ',
    'project_location' => "ยกตัวอย่าง Thailand, Panama, South Africa",
    'project_type' => 'กรุณากรอกประเภท',
    'project_start' => 'กรุณากรอกปีที่เริ่มต้น',
    'project_end' => 'กรุณากรอกปีที่เสร็จสิ้น',
    'project_developer' => 'กรุณากรอกชื่อผู้พัฒนา',
    'project_city' => 'กรุณากรอกชื่อเมือง',
    'project_country' => 'กรุณากรอกชื่อประเทศ',
    'project_facade_consultant' => 'Façade Consultant',
    'project_pattern' => 'กรุณาเลือกแพทเทิร์น',
    'project_position' => 'กรุณาเลือกตำแหน่ง',
    'extra_field' => 'กรุณากรอกชื่อฟิลด์',
    'extra_value' => 'กรุณากรอกค่า',
    // Download Management
    'download_title' => 'กรุณากรอกชื่อ',
    'download_sub_title' => 'กรุณากรอกชื่อย่อย',
    // Content Management
    'content_management_title' => 'กรุณากรอกชื่อของเนื้อหา',
    'content_management_type' => 'กรุณาเลือกประเภท',
    'content_management_link' => 'ยกตัวอย่าง http://schimmermetal.co.th/en/products_and_services/export',
    // News Management
    'news_management_title' => 'กรุณากรอกชื่อข่าว',
    // Slide
    'slide_position' => 'กรุณาเลือกตำแหน่งของสไลด์',
    // Certs
    'picture_type' => 'กรุณาเลือกประเภทของภาพ',
    // Distributors
    'distributor_type' => 'กรุณากรอกประเภทที่อยู่',
    'distributor_company' => 'กรุณากรอกชื่อบริษัท',
    'distributor_phone' => 'กรุณากรอกเบอร์โทรศัพท์',
    'distributor_fax' => 'กรุณากรอกแฟกซ์',
    'distributor_facebook' => 'กรุณากรอกเฟซบุ๊ก',
    'distributor_email' => 'กรุณากรอกอีเมล',
    'distributor_embed_url' => 'กรุณากรอก Google Map Embed Link'
];
