<?php

return [
    "all" => 'ทั้งหมด',
    "dashboard" => 'หน้าแรก',
    "upload" => 'อัพโหลด',
    "create" => 'สร้าง',
    "about_us" => 'About Us',
    "content" => 'เนื้อหา',
    "youtube" => 'ลิงก์ยูทูป',
    // Admin Management
    "admin_management" => 'จัดการผู้ดูแลระบบ',
    "admin_management_create" => 'เพิ่มผู้ดูแลระบบ',
    // Project Mangement
    "project_management" => 'จัดการโปรเจกต์',
    "project_management_all" => 'จัดการโปรเจกต์ ประเภท: All',
    "project_management_th" => 'จัดการโปรเจกต์ ประเภท: Thailand',
    "project_management_inter" => 'จัดการโปรเจกต์ ประเภท: International',
    "project_management_create" => 'เพิ่มโปรเจกต์',
    "project_management_edit" => 'แก้ไขโปรเจกต์',
    'project_example' => 'ตัวอย่างข้อมูล',
    "profile" => 'โปรไฟล์',
    // Download Management
    "download_management" => 'จัดการหน้าดาวน์โหลด',
    "download_management_create" => 'เพิ่มไฟล์ดาวน์โหลด',
    "download_management_edit" => 'แก้ไขไฟล์ดาวน์โหลด',
    // Content Management
    "content_management_first_page" => 'จัดการเนื้อหา (หน้าแรก)',
    "content_management" => 'จัดการเนื้อหา',
    "content_management_create" => 'สร้างเนื้อหา',
    "content_management_edit" => 'แก้ไขเนื้อหา',
    "first_page" => 'หน้าแรก',
    "slide" => 'สไลด์',
    "first_page_content" => 'เนื้อหา',
    "youtube_link_setup" => 'ลิงก์ยูทูบ',
    "project_reference" => 'Project Reference',
    "project_reference_create" => 'สร้าง Project Reference',
    "project_reference_edit" => 'แก้ไข Project Reference',
    // Slide Management
    "slide_management" => 'จัดการสไลด์',
    "slide_management_create" => 'สร้างสไลด์',
    "slide_management_edit" => 'แก้ไขสไลด์',
    // Certs and standards
    "certificate_and_standard" => 'Certificate & Standard',
    "certificate_and_standard_create" => 'อัพโหลด Certificate & Standard',
    "certificate_and_standard_edit" => 'แก้ไข Certificate & Standard',
    // News
    "news" => 'ข่าว',
    "add_news" => 'เพิ่มข่าว',
    "news_management" => 'จัดการข่าว',
    "news_management_create" => 'สร้างข่าว',
    "news_management_edit" => 'แก้ไขข่าว',
    // Youtube Link
    "youtube_management" => 'จัดการลิงก์ยูทูป',
    "youtube_management_edit" => 'แก้ไขลิงก์ยูทูป',
    // Certs and standard
    "cert_and_standard" => 'Certificate & Standard',
    // Content
    "extrusion" => 'Extrusion',
    "finishing" => 'Finishing',
    "extended_service" => 'Extended Service',
    // Project
    "all_project" => 'All',
    "th_project" => 'Thailand',
    "inter_project" => 'International',
    // About Us
    "about_us_management" => 'จัดการเนื้อหาหน้า About Us',
    // Distributors
    "distributor_management" => 'จัดการตัวแทนจำหน่าย',
    "distributor_management_create" => 'เพิ่มตัวแทนจำหน่าย',
];
