<?php

return [
    'submit' => 'ยืนยัน',
    'change_password' => 'เปลี่ยนรหัสผ่าน',
    'delete' => 'ลบ',
    'edit' => 'แก้ไข',
    'cancel' => 'ยกเลิก',
    'enable' => 'เปิดใช้งาน',
    'disable' => 'ปิดใช้งาน',
    'add_picture' => 'เพิ่มรูปภาพ',
    'add_news' => 'เพิ่มข่าว',
    'add_news_picture' => 'เพิ่มภาพข่าว',
    'add_project' => 'เพิ่มโปรเจกต์',
    'add_existing_project' => 'เพิ่มจากโปรเจกต์ที่มี',
    'add_field' => 'เพิ่มฟิลด์',
    'add_download_file' => 'เพิ่มไฟล์ดาวน์โหลด',
    'add_slide' => 'เพิ่มสไลด์',
    'add_distributor' => 'เพิ่มตัวแทนจำหน่าย'
];
