<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();

        // Assign role to super-admin
        Role::create(['name' => 'super-admin']);
        $user->assignRole('super-admin');
    }
}
