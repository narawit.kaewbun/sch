<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'username' => 'admin1',
                'email' => 'admin1@example.com',
                'password' => bcrypt('123456'),
            ],
            [
                'username' => 'admin2',
                'email' => 'admin2@example.com',
                'password' => bcrypt('123456'),
            ],
        ];

        DB::table('users')->insert($data);
    }
}
