<?php

use Illuminate\Database\Seeder;
use App\Youtube;

class YoutubesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          [
              'link' => 'https://www.youtube.com/watch?v=YRVrVhZaQXY',
              'type' => 'first-page'
          ],
          [
              'link' => 'https://www.youtube.com/watch?v=YRVrVhZaQXY&feature=emb_title',
              'type' => 'about-us'
          ]
        ];

        DB::table('youtubes')->insert($data);
    }
}
