<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorizedDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorized_distributors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('company_name');
            $table->string('address');
            $table->string('phone');
            $table->string('fax')->nullable();
            $table->string('facebook')->nullable();
            $table->string('email');
            $table->string('google_map_embed_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authorized_distributors');
    }
}
