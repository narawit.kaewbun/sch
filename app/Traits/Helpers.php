<?php

namespace App\Traits;

use Auth;

trait Helpers
{
    /**
     * Get the user instance
     *
     * @return \App\User|null
     */
    public function user()
    {
        return Auth::user();
    }
}
