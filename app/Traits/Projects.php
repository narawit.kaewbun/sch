<?php

namespace App\Traits;

trait Projects
{
    /**
     * Get pattern and position name by type
     *
     * @param $type
     * @return array
     */
    public function getPatternAndPositionName($type)
    {
        if ($type !== 'all') {
            $data = [
                'pattern' => "{$type}_pattern",
                'position' => "{$type}_position",
            ];
        } else {
            $data = [
                'pattern' => 'pattern',
                'position' => 'position',
            ];
        }

        return $data;
    }
}
