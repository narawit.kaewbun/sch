<?php

namespace App\Traits;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

trait Language
{
    public function currentLocale()
    {
        return LaravelLocalization::getCurrentLocale();
    }
}
