<?php

namespace App\Services;

use Storage;

class FileService
{
    /**
     * Upload image to specific folder
     *
     * @param $file
     * @param $folder
     * @return string
     * @throws \Exception
     */
    public function uploadFile($file, $folder)
    {
        try {
            $path = $file->hashName($folder);

            Storage::disk('public')->put($folder, $file);

            return Storage::url($path);
        } catch (\Exception $e) {
            throw new \Exception(trans('admin/error.upload_image'));
        }
    }

    /**
     * Delete image
     *
     * @param $file
     * @return bool
     */
    public function deleteFile($file)
    {
        $pathFile = explode('/storage/', $file)[1];

        // return true || false
        return Storage::disk('public')->delete($pathFile);
    }
}
