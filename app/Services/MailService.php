<?php

namespace App\Services;

use App\Mail\SendToAdmin;
use Mail;



class MailService
{
    public static function sendToAdmin($data)
    {
        Mail::send(new SendToAdmin($data));
    }
}
