<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\NewsRepositoryInterface;
use App\SchNew;

class NewsRepository extends Controller implements NewsRepositoryInterface
{
    public function getAll()
    {
        return SchNew::all();
    }
}
