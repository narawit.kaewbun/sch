<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\YoutubeRepositoryInterface;
use App\Youtube;

class YoutubeRepository extends Controller implements YoutubeRepositoryInterface
{
    public function getByType($type)
    {
        return Youtube::where('type', $type)->first();
    }
}
