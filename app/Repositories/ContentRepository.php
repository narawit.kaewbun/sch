<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ContentRepositoryInterface;
use App\Content;

class ContentRepository extends Controller implements ContentRepositoryInterface
{
    public function getAll()
    {
        return Content::all();
    }

    public function getByType($type, $isHidden = false)
    {
        $query = Content::where('type', $type);

        return $isHidden ?  $query->get() : $query->where('is_hidden', $isHidden)->get();
    }
}
