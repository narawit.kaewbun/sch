<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Project;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Traits\Projects;

class ProjectRepository extends Controller implements ProjectRepositoryInterface
{
    use Projects;

    public function getProjectById($id)
    {
        return Project::find($id);
    }

    public function getAllProjects()
    {
        return Project::typeAll()->get();
    }

    public function getThailandProjects()
    {
        return Project::thailand()->get();
    }

    public function getInternationalProjects()
    {
        return Project::international()->get();
    }

    public function getProjectLessBetweenOldAndNewPosition($pattern, $oldPosition, $newPosition, $type = null)
    {
        $data = $this->getPatternAndPositionName($type);

        return Project::where($data['pattern'], $pattern)->whereBetween($data['position'], [$oldPosition + 1, $newPosition])->get();
    }

    public function getProjectMoreBetweenOldAndNewPosition($pattern, $oldPosition, $newPosition, $type = null)
    {
        $data = $this->getPatternAndPositionName($type);

        return Project::where($data['pattern'], $pattern)->whereBetween($data['position'], [$newPosition, $oldPosition - 1])->get();
    }

    public function getProjectByPatternAndPosition($pattern, $position, $type = null)
    {
        $data = $this->getPatternAndPositionName($type);

        return Project::where($data['pattern'], $pattern)->where($data['position'], $position)->first();
    }

    public function getProjectsByPattern($pattern, $type)
    {
        $data = $this->getPatternAndPositionName($type);

        return Project::where($data['pattern'], $pattern)->OrderBy($data['position'])->get();
    }

    public function getProjectsWithExceptType($type)
    {
        $data = $this->getPatternAndPositionName($type);

        return Project::whereNull($data['pattern'])->whereNull($data['position'])->get();
    }
}
