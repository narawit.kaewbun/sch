<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\DownloadRepositoryInterface;
use App\Download;

class DownloadRepository extends Controller implements DownloadRepositoryInterface
{
    public function getAll()
    {
        return Download::all();
    }
}
