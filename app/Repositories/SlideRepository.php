<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\SlideRepositoryInterface;
use App\Slide;

class SlideRepository extends Controller implements SlideRepositoryInterface
{
    public function getAll()
    {
        return Slide::orderBy('position')->get();
    }

    public function getAllNotHidden()
    {
        return Slide::notHidden()->orderBy('position')->get();
    }

    public function getByPosition($position)
    {
        return Slide::where('position', $position)->first();
    }

    public function getSlideLessToMoreBetweenOldAndNewPosition($oldPosition, $newPosition)
    {
        return Slide::whereBetween('position', [$oldPosition + 1, $newPosition])->get();
    }

    public function getSlideMoreToLessBetweenOldAndNewPosition($oldPosition, $newPosition)
    {
        return Slide::whereBetween('position', [$newPosition, $oldPosition - 1])->get();
    }

    public function getSlideMoreThanOrEqualPosition($position)
    {
        return Slide::where('position', '>=', $position)->get();
    }
}
