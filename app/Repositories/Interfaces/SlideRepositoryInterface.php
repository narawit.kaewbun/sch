<?php

namespace App\Repositories\Interfaces;

interface SlideRepositoryInterface
{
    public function getAll();

    public function getAllNotHidden();

    public function getByPosition($position);

    public function getSlideLessToMoreBetweenOldAndNewPosition($oldPosition, $newPosition);

    public function getSlideMoreToLessBetweenOldAndNewPosition($oldPosition, $newPosition);

    public function getSlideMoreThanOrEqualPosition($position);
}
