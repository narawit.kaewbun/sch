<?php

namespace App\Repositories\Interfaces;

interface ProjectRepositoryInterface
{
    public function getProjectById($id);

    public function getAllProjects();

    public function getProjectLessBetweenOldAndNewPosition($pattern, $oldPosition, $newPosition, $type);

    public function getProjectMoreBetweenOldAndNewPosition($pattern, $oldPosition, $newPosition, $type);

    public function getProjectByPatternAndPosition($pattern, $position, $type);

    public function getProjectsByPattern($pattern, $type);

    public function getProjectsWithExceptType($type);
}
