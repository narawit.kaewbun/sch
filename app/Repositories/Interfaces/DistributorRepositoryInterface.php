<?php

namespace App\Repositories\Interfaces;

interface DistributorRepositoryInterface
{
    public function getAll();
}
