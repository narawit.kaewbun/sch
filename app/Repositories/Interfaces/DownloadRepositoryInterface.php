<?php

namespace App\Repositories\Interfaces;

interface DownloadRepositoryInterface
{
    public function getAll();
}
