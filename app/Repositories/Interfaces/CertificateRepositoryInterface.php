<?php

namespace App\Repositories\Interfaces;

interface CertificateRepositoryInterface {
    public function getAll();
    public function getByType($type);
}
