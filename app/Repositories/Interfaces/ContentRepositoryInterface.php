<?php

namespace App\Repositories\Interfaces;

interface ContentRepositoryInterface {
    public function getAll();

    public function getByType($type, $isHidden);
}
