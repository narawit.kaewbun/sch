<?php

namespace App\Repositories\Interfaces;

interface YoutubeRepositoryInterface
{
    public function getByType($type);
}
