<?php

namespace App\Repositories\Interfaces;

interface ProjectRefRepositoryInterface
{
    public function getAll();

    public function getAllWithProject();
}
