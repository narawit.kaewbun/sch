<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;

class UserRepository extends Controller implements UserRepositoryInterface
{
    public function getAllAdminUsers()
    {
        return User::where('role', 'admin')->where('id', '!=', $this->user()->id)->get();
    }
}
