<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\ProjectReference;
use App\Repositories\Interfaces\ProjectRefRepositoryInterface;

class ProjectRefRepository extends Controller implements ProjectRefRepositoryInterface
{
    public function getAll()
    {
        return ProjectReference::orderBy('position')->get();
    }

    public function getAllWithProject()
    {
        return ProjectReference::with('project')->orderBy('position')->get()->map(function ($item) {
            return [
                'id' => $item->project->id,
                'name' => $item->project->name,
                'location' => $item->project->location,
                'image' => $item->project->cover_image,
                'position' => $item->position
            ];
        });
    }
}
