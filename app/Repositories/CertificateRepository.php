<?php

namespace App\Repositories;

use App\Certificate;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CertificateRepositoryInterface;

class CertificateRepository extends Controller implements CertificateRepositoryInterface
{
    public function getAll()
    {
        return Certificate::all();
    }

    public function getByType($type)
    {
        return Certificate::where('type', $type)->get();
    }
}
