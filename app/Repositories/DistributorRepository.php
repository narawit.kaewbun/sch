<?php

namespace App\Repositories;

use App\AuthorizedDistributor;
use App\Repositories\Interfaces\DistributorRepositoryInterface;

class DistributorRepository implements DistributorRepositoryInterface
{
    public function getAll()
    {
        return AuthorizedDistributor::all();
    }
}
