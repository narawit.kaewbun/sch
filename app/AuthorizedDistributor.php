<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorizedDistributor extends Model
{
    protected $guarded = [];
}
