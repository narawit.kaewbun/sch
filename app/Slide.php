<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $guarded = [];

    public function scopeNotHidden($query)
    {
        return $query->where('is_hidden', 0);
    }
}
