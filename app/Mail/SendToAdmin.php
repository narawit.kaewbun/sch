<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendToAdmin extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * SendToAdmin constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->subject($data['subject']);
        $this->from($data['email']);
        $this->to(config('mail.to'));
    }

    /**
     * @return SendToAdmin
     */
    public function build()
    {
        return $this->view('mails.index');
    }
}
