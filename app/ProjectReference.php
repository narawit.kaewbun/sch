<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectReference extends Model
{
    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
