<?php

namespace App\Providers;

use App\Repositories\CertificateRepository;
use App\Repositories\ContentRepository;
use App\Repositories\DistributorRepository;
use App\Repositories\DownloadRepository;
use App\Repositories\Interfaces\CertificateRepositoryInterface;
use App\Repositories\Interfaces\ContentRepositoryInterface;
use App\Repositories\Interfaces\DistributorRepositoryInterface;
use App\Repositories\Interfaces\DownloadRepositoryInterface;
use App\Repositories\Interfaces\NewsRepositoryInterface;
use App\Repositories\Interfaces\ProjectRefRepositoryInterface;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Repositories\Interfaces\SlideRepositoryInterface;
use App\Repositories\Interfaces\YoutubeRepositoryInterface;
use App\Repositories\NewsRepository;
use App\Repositories\ProjectRefRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\SlideRepository;
use App\Repositories\UserRepository;
use App\Repositories\YoutubeRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\UserRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    private $baseInterfaceNamespace = "App\Repositories\Interfaces";
    private $baseRepositoryNameSpace = "App\Repositories";
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $interfaces = [
//            UserRepositoryInterface::class,
//            ProjectRepositoryInterface::class,
//            DownloadRepositoryInterface::class,
//            ContentRepositoryInterface::class,
//            SlideRepositoryInterface::class,
//            CertificateRepositoryInterface::class,
//            NewsRepositoryInterface::class,
//            YoutubeRepositoryInterface::class,
//            ProjectRefRepositoryInterface::class,
//        ];

        $repositories = [
            UserRepositoryInterface::class => UserRepository::class,
            ProjectRepositoryInterface::class => ProjectRepository::class,
            DownloadRepositoryInterface::class => DownloadRepository::class,
            ContentRepositoryInterface::class => ContentRepository::class,
            SlideRepositoryInterface::class => SlideRepository::class,
            CertificateRepositoryInterface::class => CertificateRepository::class,
            NewsRepositoryInterface::class => NewsRepository::class,
            YoutubeRepositoryInterface::class => YoutubeRepository::class,
            ProjectRefRepositoryInterface::class => ProjectRefRepository::class,
            DistributorRepositoryInterface::class => DistributorRepository::class,
        ];

        foreach ($repositories as $key => $value) {
            $this->app->bind($key, $value);
        }
    }
}
