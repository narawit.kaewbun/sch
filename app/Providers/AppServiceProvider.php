<?php

namespace App\Providers;

use App\Repositories\Interfaces\ContentRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Types for getting content
     *
     * @var array
     */
    private $contentTypes = [
        'extrusion',
        'finishing',
        'extended-service'
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    public function boot(ContentRepositoryInterface $contentRepository)
    {
        $menus = [];

        if (\Schema::hasTable('contents')) {
            foreach ($this->contentTypes as $contentType) {
                $menus[$contentType] = [];
                $data = $contentRepository->getByType($contentType);

                foreach ($data as $datum) {
                    $menuId = '';
                    $strings = explode(' ', $datum->en_title);

                    if (count($strings) > 1) {
                        foreach ($strings as $key => $value) {
                            if ($key === count($strings) - 1) {
                                $menuId .= $value;
                            } else {
                                $menuId .= $value . '_';
                            }
                        }
                    } else {
                        $menuId = $datum->en_title;
                    }

                    $menus[$contentType][$datum->en_title] = strtolower($menuId);
                }
            }
        }

        \View::share('menus', $menus);
    }
}
