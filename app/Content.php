<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Language;

class Content extends Model
{
    use Language;

    /**
     * Prefix language for dynamically change attribute
     *
     * @var string
     */
    private $prefixLang;

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->prefixLang = $this->currentLocale();
    }

    public function getTitleAttribute()
    {
        if ($this->prefixLang === 'en') {
            return null;
        } else {
            return $this->{$this->prefixLang . "_title"};
        }
    }

    public function getContentAttribute()
    {
        return $this->{$this->prefixLang . "_content"};
    }
}
