<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    public function scopeTypeAll($query)
    {
        return $query->whereNotNull('pattern')->orWhereNotNull('position');
    }

    public function scopeThailand($query)
    {
        return $query->whereNotNull('th_pattern')->orWhereNotNull('th_position');
    }

    public function scopeInternational($query)
    {
        return $query->whereNotNull('inter_pattern')->orWhereNotNull('inter_position');
    }
}
