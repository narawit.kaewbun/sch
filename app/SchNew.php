<?php

namespace App;

use App\Traits\Language;
use Illuminate\Database\Eloquent\Model;

class SchNew extends Model
{
    use Language;

    /**
     * Prefix language for dynamically change attribute
     *
     * @var string
     */
    private $prefixLang;

    /**
     * Set table name
     *
     * @var string
     */
    protected $table = 'news';

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->prefixLang = $this->currentLocale();
    }

    public function getTitleAttribute()
    {
        return $this->{$this->prefixLang . "_title"};
    }

    public function getContentAttribute()
    {
        return $this->{$this->prefixLang . "_content"};
    }

    /**
     * Get all images that belongs to this news
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function newImages()
    {
        return $this->hasMany('App\NewsImage', 'new_id');
    }

}
