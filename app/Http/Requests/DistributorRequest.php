<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistributorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->role === 'admin';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string',
            'company_name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string',
            'fax' => 'nullable|string',
            'facebook' => 'nullable|string',
            'email' => 'required|email',
            'google_map_embed_url' => 'required|string',
        ];
    }
}
