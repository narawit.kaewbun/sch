<?php

namespace App\Http\Controllers;

use App\Project;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    private $projectRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * Get Projects By Specific Type
     *
     * Example of getting data
     *
     * All -> route('projects.index')
     * Thailand -> route('projects.index', ['type', 'th'])
     * International -> route('projects.index', ['type', 'inter'])
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = [];
        $type = isset($request->type) && $request->type ? $request->type : 'all';

        foreach (config("patterns.{$type}") as $key => $value) {
            $data[$key] = $this->projectRepository->getProjectsByPattern($key, $type)->toArray();
        }

        return view('front.projects', compact('data'));
    }

    public function show(Project $project)
    {
        // get previous user id
        $previous =  $project::where('id', '<', $project->id)->max('id');
        // get next user id
        $next = $project::where('id', '>', $project->id)->min('id');
        
        return view('front.project-detail', compact('project'))->with('next', $next)->with('previous', $previous);
    }
}
