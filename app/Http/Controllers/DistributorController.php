<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\DistributorRepositoryInterface;
use Illuminate\Http\Request;

class DistributorController extends Controller
{
    private $distributorRepository;

    public function __construct(DistributorRepositoryInterface $distributorRepository)
    {
        $this->distributorRepository = $distributorRepository;
    }

    public function __invoke()
    {
       $distributors = $this->distributorRepository->getAll();
       // dd($distributors);
       return view('front.authorized', compact('distributors'));
    }
}
