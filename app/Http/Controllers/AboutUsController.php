<?php

namespace App\Http\Controllers;

use App\Repositories\CertificateRepository;
use App\Repositories\Interfaces\ContentRepositoryInterface;
use App\Repositories\Interfaces\YoutubeRepositoryInterface;

class AboutUsController extends Controller
{
    /**
     * Types for getting certificate and standard images
     *
     * @var array
     */
    private $certTypes = [
        'certificate',
        'standard',
    ];

    /**
     * Type name for getting content and youtube link
     *
     * @var string
     */
    private $type = 'about-us';

    private $certificateRepository;
    private $contentRepository;
    private $youtubeRepository;

    public function __construct(
        CertificateRepository $certificateRepository,
        ContentRepositoryInterface $contentRepository,
        YoutubeRepositoryInterface $youtubeRepository
    ) {
        $this->certificateRepository = $certificateRepository;
        $this->contentRepository = $contentRepository;
        $this->youtubeRepository = $youtubeRepository;
    }

    public function __invoke()
    {
        $data = [];

        $content = $this->contentRepository->getByType($this->type);
        $youtube = $this->youtubeRepository->getByType($this->type);

        foreach ($this->certTypes as $certType) {
            $data[$certType] = $this->certificateRepository->getByType($certType);
        }

        return view('front.about', compact('content', 'youtube', 'data'));
    }
}
