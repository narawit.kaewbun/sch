<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\ContentRepositoryInterface;
use App\Repositories\Interfaces\ProjectRefRepositoryInterface;
use App\Repositories\Interfaces\SlideRepositoryInterface;
use App\Repositories\Interfaces\YoutubeRepositoryInterface;

class IndexPageController extends Controller
{
    private $slideRepository;
    private $contentRepository;
    private $youtubeRepository;
    private $projectRefRepository;

    /**
     * Type name for getting content and youtube link
     *
     * @var string
     */
    private $firstPageType = 'first-page';

    public function __construct(
        SlideRepositoryInterface $slideRepository,
        ContentRepositoryInterface $contentRepository,
        YoutubeRepositoryInterface $youtubeRepository,
        ProjectRefRepositoryInterface $projectRepository
    ) {
        $this->slideRepository = $slideRepository;
        $this->contentRepository = $contentRepository;
        $this->youtubeRepository = $youtubeRepository;
        $this->projectRefRepository = $projectRepository;
    }

    public function __invoke()
    {
        $slides = $this->slideRepository->getAllNotHidden();
        $contents = $this->contentRepository->getByType($this->firstPageType);
        $youtube = $this->youtubeRepository->getByType($this->firstPageType);
        $projectRefs = $this->projectRefRepository->getAllWithProject();

        return view('front.home', compact('slides', 'contents', 'youtube', 'projectRefs'));
    }
}
