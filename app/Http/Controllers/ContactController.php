<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use App\Services\MailService;

class ContactController extends Controller
{
    public function index()
    {
        return view('front.contact');
    }

    public function submitContactForm(ContactUsRequest $request)
    {
        $data = $request->except('_token');

        MailService::sendToAdmin($data);

        return redirect()->back()->with('success', 'Your request successfully send to our system.');
    }
}
