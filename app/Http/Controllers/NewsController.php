<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\NewsRepositoryInterface;
use App\SchNew;

class NewsController extends Controller
{
    private $newsRepository;

    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    public function index()
    {
        $news = $this->newsRepository->getAll();

        return view('front.news', compact('news'));
    }

    public function show(SchNew $schNew)
    {
        // Get Images of the new
        // $schNew->newImages

        // get previous user id
        $previous =  $schNew::where('id', '<', $schNew->id)->max('id');
        // get next user id
        $next = $schNew::where('id', '>', $schNew->id)->min('id');

        return view('front.news-detail', compact('schNew'))->with('next', $next)->with('previous', $previous);
    }
}
