<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\DownloadRepositoryInterface;

class DownloadPageController extends Controller
{
    private $downloadRepository;

    public function __construct(DownloadRepositoryInterface $downloadRepository)
    {
        $this->downloadRepository = $downloadRepository;
    }

    public function index()
    {
        $downloads = $this->downloadRepository->getAll();

        return view('front.download', compact('downloads'));
    }
}
