<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\ContentRepositoryInterface;

class ProductAndServiceController extends Controller
{
    /**
     * Types for getting content
     *
     * @var array
     */
    private $contentTypes = [
        'extrusion',
        'finishing',
        'extended-service'
    ];

    private $contentRepository;

    public function __construct(ContentRepositoryInterface $contentRepository)
    {
        $this->contentRepository = $contentRepository;
    }

    public function __invoke($name = null)
    {
        $data = [];

        foreach ($this->contentTypes as $type) {
            $data[$type] = [];

            $items = $this->contentRepository->getByType($type);

            foreach ($items as $item) {
                $menuId = '';
                $strings = explode(' ', $item->en_title);

                if (count($strings) > 1) {
                    foreach ($strings as $key => $value) {
                        if ($key === count($strings) - 1) {
                            $menuId .= $value;
                        } else {
                            $menuId .= $value . '_';
                        }
                    }
                } else {
                    $menuId = $item->en_title;
                }

                $data[$type][strtolower($menuId)] = $item;
            }
        }
        
        return view('front.products', compact('data', 'name'));
    }
}
