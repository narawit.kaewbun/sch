<?php

namespace App\Http\Controllers\Admin\Content;

use App\Content;
use App\Http\Controllers\Controller;
use App\ProjectReference;
use App\Repositories\Interfaces\ContentRepositoryInterface;
use App\Repositories\Interfaces\SlideRepositoryInterface;
use App\Services\FileService;
use App\Slide;
use App\Youtube;
use DB;
use Exception;
use Illuminate\Http\Request;

class FirstPageController extends Controller
{
    private $slideRepository;
    private $contentRepository;
    private $fileService;

    public function __construct(SlideRepositoryInterface $slideRepository, FileService $fileService, ContentRepositoryInterface $contentRepository)
    {
        $this->slideRepository = $slideRepository;
        $this->contentRepository = $contentRepository;
        $this->fileService = $fileService;
    }

    public function slideAll()
    {
        $slides = $this->slideRepository->getAll();

        return view('admin.pages.contents.firstPage.slide.index', compact('slides'));
    }

    public function slideCreate()
    {

        $slideCounts = $this->slideRepository->getAll()->count();

        return view('admin.pages.contents.firstPage.slide.create', compact('slideCounts'));
    }

    public function slideStore(Request $request)
    {
        try {
            DB::beginTransaction();

            $slideData = [];
            $data = $request->except('_token');

            foreach ($data as $key => $value) {
                $strings = explode('-', $key);

                if ($strings[0] === 'slide') {
                    $slideData[$strings[1]]['position'] = $value;
                } else {
                    if ($strings[0] === 'image') {
                        $slideData[$strings[1]]['image'] = $this->fileService->uploadFile($value, 'slides');
                    }
                }
            }

            foreach ($slideData as $slideDatum) {
                if ($this->slideRepository->getByPosition($slideDatum['position']) !== null) {
                    $slides = $this->slideRepository->getSlideMoreThanOrEqualPosition($slideDatum['position']);

                    foreach ($slides as $slide) {
                        $slide->position += 1;

                        $slide->save();
                    }
                }

                Slide::create($slideDatum);
            }

            DB::commit();

            return redirect()->back()->with('success', 'สร้างสไลด์สำเร็จ');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('success', $e->getMessage());
        }
    }

    public function slideEdit(Slide $slide)
    {
        $slideCounts = $this->slideRepository->getAll()->count();

        return view('admin.pages.contents.firstPage.slide.edit', ['slide' => $slide, 'slideCounts' => $slideCounts]);
    }

    public function slideDestroy(Slide $slide)
    {
        try {
            $slide->delete();

            if ($slide->delete()) {
                $this->fileService->deleteFile($slide->image);
            }

            return redirect()->back()->with('success', 'ลบสไลด์สำเร็จ');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function slideVisibility(Slide $slide)
    {
        try {
            $slide->is_hidden = !$slide->is_hidden;

            $slide->save();

            return back()->with('success', trans('admin/success.update_visibility'));
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function slideUpdate(Slide $slide, Request $request)
    {
        $data = $request->except(['_token', '_method']);
        $oldImagePath = $slide->image;

        try {
            DB::beginTransaction();

            if (isset($request->image) && $request->image) {
                $data['image'] = $this->fileService->uploadFile($request->image, 'slides');
            }

            $this->updateSlidePositions($slide->position, $data['position']);

            if ($slide->update($data) && isset($data['image'])) {
                $this->fileService->deleteFile($oldImagePath);
            }

            DB::commit();
            return back()->with('success', trans('admin/success.update_success'));
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    private function updateSlidePositions($oldPosition, $newPosition)
    {
        // 1 > 3 ---> 3 -> 2 -> 1
        if ($oldPosition < $newPosition) {
            $slides = $this->slideRepository->getSlideLessToMoreBetweenOldAndNewPosition($oldPosition, $newPosition);

            foreach ($slides as $slide) {
                $slide->position = $slide->position - 1;
                $slide->save();
            }
            // Query only 3 and 2
        } else if ($oldPosition > $newPosition){
            $slides = $this->slideRepository->getSlideMoreToLessBetweenOldAndNewPosition($oldPosition, $newPosition);

            foreach ($slides as $slide) {
                $slide->position = $slide->position + 1;
                $slide->save();
            }
        }
    }

    public function contentAll()
    {
        $contents = $this->contentRepository->getByType('first-page');

        return view('admin.pages.contents.firstPage.content.index', compact('contents'));
    }

    public function contentCreate()
    {
        return view('admin.pages.contents.firstPage.content.create');
    }

    public function contentStore(Request $request)
    {
        try {
            $data = $request->except('_token');
            $data['type'] = 'first-page';

            $data['image'] = $this->fileService->uploadFile($data['image'], 'content');

            Content::create($data);

            return redirect()->back()->with('success', trans('admin/success.create_content'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function contentEdit(Content $content)
    {
        return view('admin.pages.contents.firstPage.content.edit', compact('content'));
    }

    public function contentUpdate(Request $request, Content $content)
    {
        try {
            $data = $request->except(['_token', '_method']);
            $data['type'] = 'first-page';

            if (isset($request->image)) {
                $data['image'] = $this->fileService->uploadFile($data['image'], 'content');

                if ($data['image']) {
                    $this->fileService->deleteFile($content->image);
                }
            }

            $content->update($data);

            return back()->with('success', trans('admin/success.update_content'));
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function youtubeIndex()
    {
        $youtube = Youtube::first();

        return view('admin.pages.contents.firstPage.youtube', compact('youtube'));
    }

    public function youtubeUpdate(Request $request, Youtube $youtube)
    {
        try {
            $youtube->update($request->all());

            return back()->with('success', trans('admin/success.update_content'));
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function projectRefIndex()
    {
        $projectsRef = ProjectReference::all();

        return view('admin.pages.contents.firstPage.projectRef.index', compact('projectsRef'));
    }

    public function projectRefCreate()
    {
        return view('admin.pages.contents.firstPage.projectRef.create');
    }

    public function projectRefStore(Request $request)
    {
        try {
            $data = $request->except(['_token']);

            $projectRef = ProjectReference::where('position', $data['position'])->first();

            if ($projectRef) {
                $projectRef->project_id = $data['project_id'];
                $projectRef->save();
            } else {
                ProjectReference::create($data);
            }

            return redirect()->back()->with('success', trans('admin/success.create_content'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function projectRefEdit(ProjectReference $projectReference)
    {
        return view('admin.pages.contents.firstPage.projectRef.edit', compact('projectReference'));
    }

    public function projectRefUpdate(Request $request, ProjectReference $projectReference)
    {
        try {
            DB::beginTransaction();

            if ($request->position > $projectReference->position) {
                $projectsRef = ProjectReference::whereBetween('position', [$projectReference->position + 1, $request->position])->get();

                foreach ($projectsRef as $projectRef) {
                    $projectRef->position -= 1;

                    $projectRef->save();
                }
            } else if ($request->position < $projectReference->position) {
                $projectsRef = ProjectReference::whereBetween('position', [$request->position, $projectReference->position - 1])->get();

                foreach ($projectsRef as $projectRef) {
                    $projectRef->position += 1;

                    $projectRef->save();
                }
            }

            $projectReference->update($request->except(['_token', '_method']));

            DB::commit();
            return back()->with('success', trans('admin/success.delete_content'));
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', $e->getMessage());
        }
    }

    public function projectRefDestroy(ProjectReference $projectReference)
    {
        try {
            $projectReference->delete();

            return back()->with('success', trans('admin/success.delete_content'));
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
