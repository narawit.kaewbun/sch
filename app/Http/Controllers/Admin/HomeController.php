<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use DB;

class HomeController extends Controller
{
    private $basedView = 'admin.pages';

    public function index()
    {
        if (!$this->user()) {
            return view("{$this->basedView}.auth.login");
        }

        return redirect(route("admin.project_management.index", 'all'));
    }
}
