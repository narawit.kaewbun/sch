<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Project;
use App\ProjectReference;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Services\FileService;
use App\Traits\Projects;
use Exception;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    use Projects;

    private $projectRepository;
    private $fileService;

    public function __construct(ProjectRepositoryInterface $projectRepository, FileService $fileService)
    {
        $this->projectRepository = $projectRepository;
        $this->fileService = $fileService;
    }

    public function index($type)
    {
        switch ($type) {
            case 'all':
                $projects = $this->projectRepository->getAllProjects();
                break;
            case 'th':
                $projects = $this->projectRepository->getThailandProjects();
                break;
            case 'inter':
                $projects = $this->projectRepository->getInternationalProjects();
                break;
        }

        return view('admin.pages.projects.index', compact('projects','type'));
    }

    public function create($type)
    {
        return view("admin.pages.projects.create", compact('type'));
    }

    public function existCreate($type)
    {
        $projects = $this->projectRepository->getProjectsWithExceptType($type);

        return view("admin.pages.projects.exist_create", compact('projects', 'type'));
    }

    public function store(CreateProjectRequest $request)
    {
        $extraFieldsRequest = array_diff_key($request->except('_token'), $request->validated());

        $finalData = $this->getExtraFieldsData($extraFieldsRequest);

        try {
            $data = $request->validated();

            $data['cover_image'] = $this->fileService->uploadFile($data['cover_image'], 'projects');
            $data['content_image'] = $this->fileService->uploadFile($data['content_image'], 'projects');

            $pattern = $data['type'] === 'all' ? 'pattern' : "{$data['type']}_pattern";
            $position = $data['type'] === 'all' ? 'position' : "{$data['type']}_position";

            Project::create([
                'name' => $data['name'],
                'location' => $data['location'],
                $pattern => $data['pattern'],
                $position => $data['position'],
                'cover_image' => $data['cover_image'],
                'content_image' => $data['content_image'],
                'data' => json_encode((object) $finalData),
            ]);

            return redirect()->back()->with('success', trans('admin/success.store_project'));
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function existStore(Request $request)
    {
        try {
            $project = $this->projectRepository->getProjectById($request->project);

            $names = $this->getPatternAndPositionName($request->type);

            $project->update([
                $names['pattern'] => $request->pattern,
                $names['position'] => $request->position
            ]);

            return redirect()->back()->with('success', 'เพิ่มตำแหน่งโปรเจกต์สำเร็จ');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function getExtraFieldsData($extraFieldRequests)
    {
        $data = [];
        $roundCount = 0;
        $doubleCount = 0;

        foreach ($extraFieldRequests as $key => $value) {
            if (strpos($key, 'extra') !== false) {
                $data[$roundCount][] = $value;
                if ($doubleCount == 0) {
                    $roundCount--;
                }
                $doubleCount++;
            } else {
                $key = explode('_duplicate_', $key)[0];

                $data[$roundCount][] = $key;
                $data[$roundCount][] = $value;
            }

            $doubleCount = $doubleCount == 2 ? 0 : $doubleCount;

            $roundCount++;
        }

        return $data;
    }

    public function edit($type, Project $project)
    {
        $duplicateTime = 0;
        $labels = [];

        $projectData = (array) json_decode($project->data);

        foreach ($projectData as $key => $value) {
            $labels[$value[0]] = 0;
        }

        foreach ($projectData as $key => $value) {
            if (array_key_exists($value[0], $labels)) {
                if ($duplicateTime > 0) {
                    $labels[$value[0]] += 1;

                    $projectData[$key][2] = $value[0].'_duplicate_'.$labels[$value[0]];
                }
                $duplicateTime++;
            }
        }

        $project->data = $projectData;

        return view("admin.pages.projects.edit", compact('project','type'));
    }

    public function update(Project $project, UpdateProjectRequest $request)
    {
        try {
            $data = $request->validated();

            $extraFieldsRequest = array_diff_key($request->except(['_method', '_token']), $request->validated());

            $finalData = $this->getExtraFieldsData($extraFieldsRequest);

            if (isset($request->cover_image)) {
                $data['cover_image'] = $this->fileService->uploadFile($data['cover_image'], 'projects');

                if ($data['cover_image']) {
                    $this->fileService->deleteFile($project->cover_image);
                }
            }

            if (isset($request->content_image)) {
                $data['content_image'] = $this->fileService->uploadFile($data['content_image'], 'projects');

                if ($data['content_image']) {
                    $this->fileService->deleteFile($project->cover_image);
                }
            }

            $names = $this->getPatternAndPositionName($request->type);

            if ((isset($data['pattern']) && isset($data['position'])) && ($data['pattern'] !== $project[$names['pattern']] || $data['position'] !== $project[$names['position']])) {
                $this->updatePattern($project[$names['pattern']], $data['pattern'], $project[$names['position']], $data['position'], $request->type);
            }

            $project->update([
                'name' => $data['name'],
                $names['pattern'] => isset($data['pattern']) ? $data['pattern'] : null,
                $names['position'] => isset($data['position']) ? $data['position'] : null,
                'cover_image' => isset($data['cover_image']) ? $data['cover_image'] : $project->cover_image,
                'content_image' => isset($data['content_image']) ? $data['content_image'] : $project->content_image,
                'data' => json_encode((object) $finalData),
            ]);

            return redirect()->back()->with('success', trans('admin/success.update_project'));
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    function updatePattern($oldPattern, $newPattern, $oldPosition, $newPosition, $type)
    {
        $positionsInPattern = [];

        $names = $this->getPatternAndPositionName($type);

        if ($oldPattern !== $newPattern) {
            $duplicateProject = $this->projectRepository->getProjectByPatternAndPosition($newPattern, $newPosition, $type);

            if ($duplicateProject) {

                $projects = $this->projectRepository->getProjectsByPattern($newPattern, $type);

                foreach ($projects as $project) {
                    if (in_array($project[$names['position']], config("patterns.{$type}")[$newPattern])) {
                        $positionsInPattern[] = $project[$names['position']];
                    }
                }

                $positionAvailable = array_diff(config("patterns.{$type}")[$newPattern], $positionsInPattern);

                if (count($positionAvailable)) {
                    $duplicateProject[$names['position']] = $positionAvailable[array_keys($positionAvailable)[0]];
                } else {
                    $duplicateProject[$names['position']] = null;
                }

                $duplicateProject->save();

            }
        } else {
            if ($oldPosition !== $newPosition) {
                if ($newPosition > $oldPosition) {
                    $projects = $this->projectRepository->getProjectLessBetweenOldAndNewPosition($newPattern,
                        $oldPosition, $newPosition, $type);

                    foreach ($projects as $project) {
                        $project[$names['position']] -= 1;
                        $project->save();
                    }
                } else {
                    $projects = $this->projectRepository->getProjectMoreBetweenOldAndNewPosition($newPattern,
                        $oldPosition, $newPosition, $type);

                    foreach ($projects as $project) {
                        $project[$names['position']] += 1;
                        $project->save();
                    }
                }
            }
        }
    }

    public function destroy(Project $project)
    {
        try {
            $projectRef = ProjectReference::where('project_id', $project->id)->first();
            
            if ($projectRef->delete() && $project->delete()) {
                $this->fileService->deleteFile($project->cover_image);
                $this->fileService->deleteFile($project->content_image);
            }
            
            return redirect()->back()->with('success', trans('admin/success.delete_project'));
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

}
