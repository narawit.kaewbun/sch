<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminRequest;
use App\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Http\Requests\Users\ChangePasswordRequest;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $users = $this->userRepository->getAllAdminUsers();

        return view('admin.pages.users.admin.index', compact('users'));
    }

    public function create()
    {
        return view('admin.pages.users.admin.create-admin');
    }


    public function store(CreateAdminRequest $request)
    {
        try {
            $user = User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => $request->password
            ]);

            if ($user) {
                return redirect()->back()->with('success', "New admin has been created");
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function changePassword(User $user, ChangePasswordRequest $request)
    {
        try {
            $data = $request->validated();

            $user->password = $data['new_password'];

            $user->save();

            return redirect()->back()->with('success', trans('admin/success.change_password_user'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function destroy(User $user)
    {
        try {
            $user->delete();

            return redirect()->back()->with('success', trans('admin/success.delete_user'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
