<?php

namespace App\Http\Controllers\Admin;

use App\Certificate;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CertificateRepositoryInterface;
use App\Services\FileService;
use Illuminate\Http\Request;
use DB;

class CertificateController extends Controller
{
    private $fileService;
    private $certificateRepository;

    public function __construct(FileService $fileService, CertificateRepositoryInterface $certificateRepository)
    {
        $this->fileService = $fileService;
        $this->certificateRepository = $certificateRepository;
    }

    public function index()
    {
        $certificatesAndStandards = $this->certificateRepository->getAll();

        return view('admin.pages.certs.index', compact('certificatesAndStandards'));
    }

    public function create()
    {
        return view('admin.pages.certs.create');
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $certsData = [];
            $data = $request->except('_token');

            foreach ($data as $key => $value) {
                $strings = explode('-', $key);

                if ($strings[0] === 'type') {
                    $certsData[$strings[1]]['type'] = $value;
                } else {
                    if ($strings[0] === 'image') {
                        $certsData[$strings[1]]['image'] = $this->fileService->uploadFile($value, 'certs');
                    }
                }
            }

            DB::table('certificates')->insert($certsData);

            DB::commit();

            return redirect()->back()->with('success', 'สร้างสำเร็จแล้ว');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('success', $e->getMessage());
        }
    }

    public function edit(Certificate $certificate)
    {
        return view('admin.pages.certs.edit', compact('certificate'));
    }

    public function update(Request $request, Certificate $certificate)
    {
        try {
            DB::beginTransaction();

            $data = $request->except(['_token', '_method']);
            $oldImagePath = $certificate->image;

            if (isset($request->image) && $request->image) {
                $data['image'] = $this->fileService->uploadFile($request->image, 'certs');
            }

            if ($certificate->update($data) && isset($data['image'])) {
                $this->fileService->deleteFile($oldImagePath);
            }

            DB::commit();
            return back()->with('success', trans('admin/success.update_success'));
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('success', trans('admin/success.update_success'));
        }
    }

    public function destroy(Certificate $certificate)
    {
        try {
            $certificate->delete();

            return back()->with('success', trans('admin/success.update_success'));
        } catch (\Exception $e) {
            return back()->with('success', trans('admin/success.update_success'));
        }
    }
}
