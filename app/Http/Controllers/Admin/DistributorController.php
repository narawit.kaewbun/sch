<?php

namespace App\Http\Controllers\Admin;

use App\AuthorizedDistributor;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\DistributorRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Requests\DistributorRequest;

class DistributorController extends Controller
{
    private $distributorRepository;

    public function __construct(DistributorRepositoryInterface $distributorRepository)
    {
        $this->distributorRepository = $distributorRepository;
    }

    public function index()
    {
        $distributors = $this->distributorRepository->getAll();

        return view('admin.pages.distributors.index', compact('distributors'));
    }

    public function create()
    {
        return view('admin.pages.distributors.create');
    }

    public function store(DistributorRequest $request)
    {
        $data = $request->validated();

        try {
            AuthorizedDistributor::create($data);

            return redirect()->back()->with('success', trans('admin/success.create_distributor'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit(AuthorizedDistributor $authorizedDistributor)
    {
        return view('admin.pages.distributors.edit', ['distributor' => $authorizedDistributor]);
    }

    public function update(AuthorizedDistributor $authorizedDistributor, DistributorRequest $request)
    {
        $data = $request->validated();

        try {
            $authorizedDistributor->update($data);

            return redirect()->back()->with('success', trans('admin/success.update_distributor'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function destroy(AuthorizedDistributor $authorizedDistributor)
    {
        try {
            $authorizedDistributor->delete();

            return redirect()->back()->with('success', trans('admin/success.delete_distributor'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
