<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use App\Http\Controllers\Controller;
use App\Http\Requests\AboutUsContentRequest;
use App\Http\Requests\CreateContentRequest;
use App\Http\Requests\UpdateContentRequest;
use App\Repositories\Interfaces\ContentRepositoryInterface;
use App\Repositories\Interfaces\YoutubeRepositoryInterface;
use App\Services\FileService;
use App\Youtube;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    private $fileService;
    private $contentRepository;
    private $youtubeRepository;
    private $contentTypes = [
        'extrusion',
        'finishing',
        'extended-service',
    ];

    public function __construct(
        ContentRepositoryInterface $contentRepository,
        YoutubeRepositoryInterface $youtubeRepository,
        FileService $fileService
    ) {
        $this->contentRepository = $contentRepository;
        $this->youtubeRepository = $youtubeRepository;
        $this->fileService = $fileService;
    }

    public function index($type)
    {
        $contents = $this->contentRepository->getByType($type, true);

        return view('admin.pages.contents.index', compact('contents', 'type'));
    }

    public function create($type)
    {
        return view('admin.pages.contents.create', ['types' => $this->contentTypes, 'type' => $type]);
    }

    public function store(CreateContentRequest $request)
    {
        try {
            $data = $request->validated();

            $data['image'] = $this->fileService->uploadFile($data['image'], 'content');

            Content::create($data);

            return redirect()->back()->with('success', trans('admin/success.create_content'));
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit($type, Content $content)
    {
        return view('admin.pages.contents.edit',
            ['content' => $content, 'types' => $this->contentTypes, 'type' => $type]);
    }

    public function update(Content $content, UpdateContentRequest $request)
    {
        try {
            $data = $request->validated();

            if (isset($request->image)) {
                $data['image'] = $this->fileService->uploadFile($data['image'], 'content');

                if ($data['image']) {
                    $this->fileService->deleteFile($content->image);
                }
            }

            $content->update($data);

            return back()->with('success', trans('admin/success.update_content'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function destroy(Content $content)
    {
        try {
            if ($content->delete()) {
                $this->fileService->deleteFile($content->image);
            }

            return back()->with('success', trans('admin/success.delete_content'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function handlingVisibility(Content $content)
    {
        try {
            $content->is_hidden = !$content->is_hidden;

            $content->save();

            return back()->with('success', trans('admin/success.update_visibility'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function aboutUsIndex()
    {
        $content = $this->contentRepository->getByType('about-us', true);

        if (count($content)) {
            $content = (object)$content[0];
        }

        return view('admin.pages.contents.about_us', compact('content'));
    }

    public function aboutUsUpdate(Content $content, AboutUsContentRequest $request)
    {
        $data = $request->validated();
        $className = Content::class;

        try {
            if ($content instanceof $className && $content->id) {
                if (isset($request->image)) {
                    $data['image'] = $this->fileService->uploadFile($data['image'], 'content');

                    if ($data['image']) {
                        $this->fileService->deleteFile($content->image);
                    }
                }

                $content->update($data);
            } else {
                $data['image'] = $this->fileService->uploadFile($data['image'], 'content');

                Content::create($data);
            }

            return redirect()->back()->with('success', trans('admin/success.update_content'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function aboutUsYoutube()
    {
        $youtube = $this->youtubeRepository->getByType('about-us');

        return view('admin.pages.contents.about_us_youtube', compact('youtube'));
    }

    public function aboutUsYoutubeUpdate(Youtube $youtube, Request $request)
    {
        $className = Youtube::class;

        try {
            if ($youtube instanceof $className && $youtube->id) {
                $youtube->update([
                    'link' => $request->link
                ]);
            } else {
                Youtube::create([
                    'link' => $request->link,
                    'type' => $request->type
                ]);
            }

            return redirect()->back()->with('success', trans('admin/success.youtube_update_success'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
