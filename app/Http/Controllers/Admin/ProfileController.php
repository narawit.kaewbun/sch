<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use Hash;
use Auth;

class ProfileController extends Controller
{
    public function index()
    {
        return view('admin.pages.settings.profile');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $data = $request->validated();

        if (!Hash::check($data['current_password'], $this->user()->password)) {
            return redirect()->back()->with('error', trans('admin/error.current_password'));
        }

        $this->user()->password = $data['new_password'];

        $this->user()->save();

        Auth::logout();

        return redirect('admin/login')->with('success', trans('admin/success.change_password'));
    }
}
