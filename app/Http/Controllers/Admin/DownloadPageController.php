<?php

namespace App\Http\Controllers\Admin;

use App\Download;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDownloadPageRequest;
use App\Repositories\Interfaces\DownloadRepositoryInterface;
use App\Services\FileService;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class DownloadPageController extends Controller
{
    private $downloadRepository;
    private $fileService;
    private $fileFolder = 'downloads/files';
    private $coverFolder = 'downloads/covers';

    public function __construct(DownloadRepositoryInterface $downloadRepository, FileService $fileService)
    {
        $this->downloadRepository = $downloadRepository;
        $this->fileService = $fileService;
    }

    public function index()
    {
        $downloads = $this->downloadRepository->getAll();

        return view('admin.pages.downloads.index', compact('downloads'));
    }

    public function create()
    {
        return view('admin.pages.downloads.create');
    }

    public function store(CreateDownloadPageRequest $request)
    {
        try {
            $data = $request->validated();

            $data['cover'] = $this->fileService->uploadFile($data['cover'], $this->coverFolder);
            $data['file'] = $this->fileService->uploadFile($data['file'], $this->fileFolder);

            Download::create($data);

            return redirect()->back()->with('success', trans('admin/success.create_download_page'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit(Download $download)
    {
        return view('admin.pages.downloads.edit', compact('download'));
    }

    public function update(Download $download, Request $request)
    {
        $data = $request->all();

        try {
            if (isset($data['file'])) {
                $data['file'] = $this->fileService->uploadFile($data['file'], $this->fileFolder);

                if ($data['file']) {
                    $this->fileService->deleteFile($download->file);
                }
            }

            if (isset($data['cover'])) {
                $data['cover'] = $this->fileService->uploadFile($data['cover'], $this->coverFolder);

                if ($data['cover']) {
                    $this->fileService->deleteFile($download->cover);
                }
            }

            $download->update($data);

            return redirect()->back()->with('success', trans('admin/success.download_update'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function destroy(Download $download)
    {
        try {
            if ($download->delete()) {
                $this->fileService->deleteFile($download->cover);
                $this->fileService->deleteFile($download->file);
            }

            return redirect()->back()->with('success', trans('admin/success.delete_download'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
