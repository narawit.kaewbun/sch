<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use App\Traits\Projects;
use Illuminate\Http\Request;

class PatternController extends Controller
{
    use Projects;

    public function index(Request $request)
    {
        $data = $request->all();

        if (!$data['pattern']) {
            return [];
//            $pattern = array_keys($this->patterns)[0];
        }

        $positions = config("patterns.{$data['type']}")[$data['pattern']];

        // This is for returning position not in used
        if ($data['is_in_used'] === 'false') {
            $alreadyUsedPositions = $this->getAlreadyUesdPositions($data['pattern'], $positions, $data['type']);

            $positions = array_diff($positions, $alreadyUsedPositions);
        }

        return $positions;
    }

    private function getAlreadyUesdPositions($pattern, $positions, $type)
    {
        $alreadyUsedPositions = [];

        $names = $this->getPatternAndPositionName($type);

        Project::where($names['pattern'], $pattern)
            ->whereIn($names['position'], $positions)
            ->get()
            ->map(function ($project) use (&$alreadyUsedPositions, $names) {
                array_push($alreadyUsedPositions, (int) $project[$names['position']]);

                return $project;
            });

        return $alreadyUsedPositions;
    }
}
