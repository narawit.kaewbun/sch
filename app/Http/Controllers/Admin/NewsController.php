<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\NewsImage;
use App\Repositories\Interfaces\NewsRepositoryInterface;
use App\SchNew;
use App\Services\FileService;
use DB;
use DOMDocument;
use Exception;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class NewsController extends Controller
{
    private $newsRepository;
    private $fileService;

    public function __construct(NewsRepositoryInterface $newsRepository, FileService $fileService)
    {
        $this->newsRepository = $newsRepository;
        $this->fileService = $fileService;
    }

    public function index()
    {
        $news = $this->newsRepository->getAll();

        return view('admin.pages.news.index', compact('news'));
    }

    public function create()
    {
        return view('admin.pages.news.create');
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();

            DB::beginTransaction();

            $schNew = SchNew::create([
                'th_title' => $data['th_title'],
                'en_title' => $data['en_title'],
                'th_content' => $this->handleSummerNoteEditorUpload($data['th_content'])->saveHTML(),
                'en_content' => $this->handleSummerNoteEditorUpload($data['en_content'])->saveHTML(),
                'cover' => $data['cover'] = $this->fileService->uploadFile($data['cover'], 'news'),
            ]);

            foreach ($data as $key => $value) {
                $strings = explode('-', $key);

                if ($strings[0] === 'image') {
                    $schNew->newImages()->create([
                        'image' => $this->fileService->uploadFile($value, 'news'),
                    ]);
                }
            }

            DB::commit();

            return redirect()->back()->with('success', trans('admin/success.create_news_success'));
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    private function handleSummerNoteEditorUpload($data)
    {
        $dom = new DOMDocument();

        libxml_use_internal_errors(true);

        $dom->loadHtml(mb_convert_encoding($data, 'HTML-ENTITIES', 'UTF-8'),
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');

        // foreach <img> in the submited message
        foreach ($images as $img) {
            $src = $img->getAttribute('src');

            // if the img source is 'data-url'
            if (preg_match('/data:image/', $src)) {

                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];

                // Generating a random filename
                $filename = uniqid();
                $filepath = "/images/$filename.$mimetype";

                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                    // resize if required
                    /* ->resize(300, 200) */
                    ->encode($mimetype, 100)    // encode file to the specified mimetype
                    ->save(public_path($filepath));

                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!--endforeach

        return $dom;
    }

    public function edit(SchNew $schNew)
    {
        return view('admin.pages.news.edit', compact('schNew'));
    }

    public function destroy(SchNew $schNew)
    {
        try {
            DB::beginTransaction();

            if ($schNew->delete()) {
                foreach ($schNew->newImages as $newImage) {
                    $newImage->delete();
                }
            }

            DB::commit();

            return redirect()->back()->with('success', trans('admin/success.delete_news'));
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function update(Request $request, SchNew $schNew)
    {
        try {

            DB::beginTransaction();

            $data = $request->all();

            if (isset($data['cover'])) {
                $data['cover'] = $this->fileService->uploadFile($data['cover'], 'news');

                if ($data['cover']) {
                    $this->fileService->deleteFile($schNew->cover);
                }
            }

            foreach ($data as $key => $value) {
                $strings = explode('-', $key);

                if ($strings[0] === 'old_image') {
                    $newsImage = $schNew->newImages()->where('id', $strings[1])->first();

                    if ($this->fileService->deleteFile($newsImage->image)) {
                        $newsImage->image = $this->fileService->uploadFile($value, 'news');
                    }

                    $newsImage->save();
                } elseif ($strings[0] === 'image') {
                    $schNew->newImages()->create([
                        'image' => $this->fileService->uploadFile($value, 'news'),
                    ]);
                }
            }

            $schNew->update([
                'th_title' => $data['th_title'],
                'en_title' => $data['en_title'],
                'th_content' => $this->handleSummerNoteEditorUpload($data['th_content'])->saveHTML(),
                'en_content' => $this->handleSummerNoteEditorUpload($data['en_content'])->saveHTML(),
                'cover' => isset($data['cover']) ? $data['cover'] : $schNew->cover,
            ]);

            DB::commit();

            return redirect()->back()->with('success', trans('admin/success.news_update'));
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function deleteImage(NewsImage $newsImage)
    {
        try {
            if ($this->fileService->deleteFile($newsImage->image)) {
                $newsImage->delete();
            }

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
